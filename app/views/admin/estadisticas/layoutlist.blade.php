@extends ('admin/layout')

@section ('title') Lista de {{ $nombreEstadistica }} @stop

@section ('breadcrumbs') 
    {{Breadcrumbs::render('estadisticas/page', $nombreModelo)}}
@stop

@section ('content')

    <h1 class="text-danger">{{ $nombreEstadistica }}</h1>
    <div class="panel panel-danger">
      <!-- Default panel contents -->
  <div class="panel-heading col-md-12">
    <h2 class="col-md-10">.</h2> 
    <div class="col-md-2">
      {{Form::open(array('url' => 'admin/estadisticas/'.$nombreModelo, 'method' => 'post'))}}
        {{ Form::button('Descargar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}  
        @if ($nombreModelo == 'pendientes')
            {{ Form::hidden('importacion_id', $importacion_id)}}
        @else
          {{ Form::hidden('fechaInicio', $fechaInicio)}}
          {{ Form::hidden('fechaFinal', $fechaFinal)}}
        @endif

      {{ Form::close() }}
    </div>
  </div>
      <div class="panel-body">
        <table class="table table-striped" style="font-size:14px;">
        @if($nombreModelo != 'pendientes')
          <tr>
            @foreach ($totales as $total)
                  <th>{{$total['nombre']}}</th> 
            @endforeach
          </tr>
          <tr>
          @foreach ($totales as $total)
                <td>{{$total['valor']}}</td>
          @endforeach
          </tr>
        @else
          <tr>
            @foreach ($titulosTotales as $titulo)
                  <th>{{$titulo}}</th> 
            @endforeach
          </tr>
          <tr>
          @foreach ($atributosTotales as $atributo)
                <td>{{$modeloTotales[$atributo]}}</td>
          @endforeach
          </tr>
        @endif
        </table>
      </div>
    
     	<table class="table table-striped" style="font-size:12px;">
        <tr>
            @foreach ($nombresAtributos as $nombre)
              <th>{{$nombre}}</th>  
            @endforeach
        </tr>
        @foreach ($modelos as $modelo)
        <tr>
            @foreach ($atributos as $atributo)
              @if($atributo == 'produccion')
                <td>$ {{number_format($modelo->$atributo, 2, ',', '.')}}</td>
              @elseif($atributo == 'recuperacion')
                <td>{{number_format($modelo->$atributo, 1, ',', '.')}} W</td>
              @else
                <td>{{$modelo->$atributo}}</td> 
              @endif
                
            @endforeach
        </tr>
        @endforeach
      </table>
    </div>
    @if($nombreModelo != 'pendientes')
      {{ $modelos->appends(array('fechaInicio' => $fechaInicio, 'fechaFinal' => $fechaFinal))->links() }}
    @else
      {{ $modelos->appends(array('importacion' => $importacion_id))->links() }}
    @endif

@stop