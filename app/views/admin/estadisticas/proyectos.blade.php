@extends ('admin/layout')

@section ('title') Estadistica - Proyectos Sypelc @stop

@section ('breadcrumbs') 
    {{Breadcrumbs::render('estadisticas/page', 'proyectos')}}
@stop

@section ('content')

	<h1 class="text-danger">Proyectos Sypelc</h1>
		<div class="panel panel-danger">
			<!-- Default panel contents -->
			 <div class="panel-heading col-md-12">
			    <h2 class="col-md-10">.</h2> 
			    <div class="col-md-2">
			      {{Form::open(array('url' => 'admin/estadisticas/proyectos', 'method' => 'post'))}}
			      	{{ Form::hidden('fechaInicio', $fechaInicio)}}
			      	{{ Form::hidden('fechaFinal', $fechaFinal)}}
			        {{ Form::button('Descargar', array('type' => 'submit', 'class' => 'btn btn-primary')) }}  
			      {{ Form::close() }}
			    </div>
			</div>
		
			<table class="table table-hover " style="width: 100%">
			    <tr class="active">
			          <th>Proyecto</th>   
			          <th>Asignadas</th>
			          <th>Ejecutadas Sypelc</th>
			          <th>Ejecutadas Emsa</th>
			          <th>Produccion</th>
			          <th>Recuperacion</th>
			    </tr>
			    @foreach ($datosporproyecto as $proyecto)
			    	<tr class="">
			    		<td>{{$proyecto->proyecto_id }}</td> 
				        <td>{{$proyecto->asignadas}}</td> 
				        <td>{{$proyecto->ejecutadassypelc}}</td>
				        <td>{{$proyecto->ejecutadasemsa}}</td>
						<td>$ {{number_format($proyecto->produccion, 2, ',', '.')}}</td>
				        <td>{{number_format($proyecto->recuperacion, 1, ',', '.')}} W</td>
			   		</tr>
			    @endforeach
			</table>
		</div>

	{{ $datosporproyecto->appends(array('fechaInicio' => $fechaInicio, 'fechaFinal' => $fechaFinal))->links() }}
@stop