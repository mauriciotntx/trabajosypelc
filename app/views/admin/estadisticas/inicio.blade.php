@extends ('admin/layout')

@section ('title') Estadisticas Sypelc @stop

@section ('breadcrumbs')
	{{Breadcrumbs::render(Route::currentRouteName())}}
@stop

@section ('content')
<h1 class="text-danger">Estadisticas Sypelc</h1>

<div class="panel panel-danger">
	<div class="panel-heading">Estadisticas</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
				  <!-- Default panel contents -->
				  <div class="panel-heading">Datos de Tecnicos</div>
					<div class="panel-body">
					    <p>En esta sección mira la efectividad y la producción de los tecnicos de SYPELC</p>
					    </p>
					   {{Form::open(array('url' => 'admin/estadisticas/tecnicos', 'method' => 'get'))}}
					    <div id="content-fecha" class="col-md-12 form-group">
							<div id="datepicker" class="input-daterange input-group">
								{{Form::text('fechaInicio', '01/01/2012', array('class' => 'form-control datepicker input-sm', 'data-date-format' => 'dd/mm/yyyy', 'placeholder' => 'Fecha Inicio'))}}
								<span class="input-group-addon">Hasta</span>
								{{Form::text('fechaFinal', '01/01/2015', array('class' => 'form-control datepicker input-sm', 'data-date-format' => 'dd/mm/yyyy', 'placeholder' => 'Fecha Final'))}}
								
							</div>
						</div>
					    {{ Form::button('Ver Tecnicos', array('type' => 'submit', 'class' => 'btn btn-primary')) }}  

					    {{ Form::close() }}
					</div>
				</div>	
			</div>	
			<div class="col-md-6">
				<div class="panel panel-default">
				  <!-- Default panel contents -->
				  <div class="panel-heading">Datos por Proyecto</div>
					<div class="panel-body">
					    <p>En esta sección mira la efectividad y la producción por Proyectos de SYPELC</p>
					    </p>
					  {{Form::open(array('url' => 'admin/estadisticas/proyectos', 'method' => 'get'))}}
					    <div id="content-fecha" class="col-md-12 form-group">
							<div id="datepicker" class="input-daterange input-group">
								{{Form::text('fechaInicio', '01/01/2012', array('class' => 'form-control datepicker input-sm', 'data-date-format' => 'dd/mm/yyyy', 'placeholder' => 'Fecha Inicio'))}}
								<span class="input-group-addon">Hasta</span>
								{{Form::text('fechaFinal', '01/01/2015', array('class' => 'form-control datepicker input-sm', 'data-date-format' => 'dd/mm/yyyy', 'placeholder' => 'Fecha Final'))}}
								
							</div>
						</div>
					  {{ Form::button('Ver Proyectos', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
					  {{Form::close()}}
					</div>
				</div>	
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
				  <!-- Default panel contents -->
				  <div class="panel-heading">Ordenes Pendientes</div>
					  <div class="panel-body">
					    <p>Mira las Ordenes Pendientes generadas por EMSA 
					    	y comparalas con las revisiones de Sypelc
					    </p>
					    {{Form::open(array('url' => 'admin/estadisticas/pendientes', 'method' => 'get', 'class' => 'form-inline'))}}
						    <div class="form-group">
					    		{{Form::select('importacion', $revision)}}
					 		</div>
					 		<div class="form-group">
					 			{{Form::button('Ver Ordenes Pendientes', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
					 		</div>
					    {{ Form::close() }}
					  </div>
				</div>	
			</div>
			<div class="col-md-6">
				<div class="panel panel-default">
				  <!-- Default panel contents -->
				  <div class="panel-heading">Ordenes</div>
					  <div class="panel-body">
					    <p>En esta sección mira los datos por de SYPELC</p>
					    {{Form::open(array('url' => 'admin/estadisticas/ordenes', 'method' => 'get'))}}
					    
					    <div id="content-fecha" class="col-md-12 form-group">
							<div id="datepicker" class="input-daterange input-group">
								{{Form::text('fechaInicio', '01/01/2012', array('class' => 'form-control datepicker input-sm', 'data-date-format' => 'dd/mm/yyyy', 'placeholder' => 'Fecha Inicio'))}}
								<span class="input-group-addon">Hasta</span>
								{{Form::text('fechaFinal', '01/01/2015', array('class' => 'form-control datepicker input-sm', 'data-date-format' => 'dd/mm/yyyy', 'placeholder' => 'Fecha Final'))}}
								
							</div>
						</div>

					    {{ Form::button('Ver Ordenes', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
					    {{ Form::close() }}
					</div>
				</div>	
			</div>
			
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-default">
				  <!-- Default panel contents -->
				  <div class="panel-heading">Ordenes con Fraude</div>
					  <div class="panel-body">
					    <p>Mira los fraudes generados por EMSA 
					    	en un rango de fechas determinado
					    </p>
					    {{Form::open(array('url' => 'admin/estadisticas/fraudes', 'method' => 'get'))}}
					    
					    <div id="content-fecha" class="col-md-12 form-group">
							<div id="datepicker" class="input-daterange input-group">
								{{Form::text('fechaInicio', '01/01/2012', array('class' => 'form-control datepicker input-sm', 'data-date-format' => 'dd/mm/yyyy', 'placeholder' => 'Fecha Inicio'))}}
								<span class="input-group-addon">Hasta</span>
								{{Form::text('fechaFinal', '01/01/2015', array('class' => 'form-control datepicker input-sm', 'data-date-format' => 'dd/mm/yyyy', 'placeholder' => 'Fecha Final'))}}
								
							</div>
						</div>

					    {{ Form::button('Ver Fraudes EMSA', array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
					    {{ Form::close() }}
					  </div>
				</div>	
			</div>
		</div>
	</div>
</div>

@stop