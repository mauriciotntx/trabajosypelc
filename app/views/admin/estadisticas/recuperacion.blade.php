@extends ('admin/layout')

@section ('title') Estadistica - Esados Pendientes @stop

@section ('content')

<header class="row">
	<h2 class="text-primary">Ordenes de Revision Pendientes</h2>
</header>
<div class="row">
    <div class="col-md-12">
		<table class="table table-hover " style="width: 90%">
		    <tr>
		          <th>numeroOrden</th>  
		          <th>Proyecto</th>
		          <th>Aforo</th>
		          <th>Fecha Atencion</th>
		          <th>tecnico</th>
		          <th>Recuperacion</th>
		    </tr>
		    @foreach ($revisionesSypelc as $revision)
			    <tr>
			        <td>{{$revision->id}}</td> 
			        <td>{{OrdenRevision::find($revision->id)->idProyecto}}</td>
			        <td>{{$revision->aforo}}</td>
			        <td>{{$revision->fechaAtencion}}</td> 
			        <td>{{Tecnico::find($revision->idTecnico)->nombre}}</td>
			        @if (Cliente::find(OrdenRevision::find($revision->id)->idCliente)->servicio == 'FM')
						<td>{{$revision->aforo * 24 * 30 * 5 * 0.1}}</td>
					@else
						<td>{{$revision->aforo * 24 * 30 * 5 * 0.2}}</td>
			        @endif

			    </tr>
		    @endforeach
		</table>
		{{$revisionesSypelc->links() }}
    </div>	
</div>
@stop