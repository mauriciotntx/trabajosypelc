@extends ('admin/layout')

@section ('title') Estadistica - Tecnicos Sypelc @stop

@section ('content')

<header class="row">
	<h2 class="text-primary">Tecnicos Sypelc</h2>
</header>
<div class="row">
    <div class="col-md-12">
    	
		<div class="panel panel-primary">
			<!-- Default panel contents -->
			<div class="panel-heading">Datos de Tecnicos Sypelc</div>
			<div class="panel-body">
				
			</div>
		
			<table class="table table-hover " style="width: 100%">
			    <tr class="active">
			          <th>Codigo</th>  
			          <th>Nombre</th>     
			          <th>número Actividades</th>
			          <th>número Fraudes</th>
			          <th>Producción</th>
			    </tr>
			    @foreach ($tecnicos as $tecnico)
			    	<tr class="">
				        <td>{{$tecnico->id}}</td> 
				        <td>{{$tecnico->nombre}}</td>
				        <td>{{$tecnico->cantActividades}}</td> 
				        <td>{{$tecnico->cantFraudes}}</td>
				        <td>{{money_format('%.2n',$tecnico->produccion)}}</td>
			   		</tr>
			    @endforeach
			</table>
		</div>

		{{ $tecnicos->links() }}
    </div>	
</div>
@stop