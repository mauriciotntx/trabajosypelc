@extends ('admin/layout')

@section ('title') Estadistica - Fraudes EMSA @stop

@section ('content')

<header class="row">
	<h2 class="text-primary">Fraudes EMSA</h2>
</header>
<div class="row">
    <div class="col-md-12">
    	
		<div class="panel panel-primary">
			<!-- Default panel contents -->
			<div class="panel-heading">Porcentaje de Fraudes</div>
			<div class="panel-body">
				<p>Cantidad de Fraudes: {{$cantfraudes}}</p>
				<p>Cantidad de Revisiones: {{$cantOrdenes}}</p>
			    <p styles="font-size:14px;">Hay {{round($porcentaje, 5)}} de fraudes, respecto a los generados</p>
			</div>
		
			<table class="table table-hover " style="width: 100%">
			    <tr class="active">
			          <th>numeroOrden</th>  
			          <th>fechaGeneracion</th>     
			          <th>Proyecto</th>
			    </tr>
			    @foreach ($revisiones as $revision)
			    	<tr class="">
				        <td>{{$revision->id}}</td> 
				        <td>{{$revision->fechaRealizacion}}</td> 
				        <td>{{$revision->idProyecto}}</td>
			   		</tr>
			    @endforeach
			</table>
		</div>

		{{ $revisiones->links() }}
    </div>	
</div>
@stop