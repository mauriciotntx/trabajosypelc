@extends ('admin/layout')

@section ('title') Estadistica - Esados Pendientes @stop

@section ('content')
<header class="row">
	<h2 class="text-primary">Ordenes de Revision Pendientes</h2>
</header>
<div class="row">
    <div class="col-md-12">
    
		<table class="table table-hover " style="width: 90%">
		    <tr class="active">
		          <th>numeroOrden</th>  
		          <th>Proyecto</th>
		          <th>fechaGeneracion</th>
		          <th>Pendiente</th>
		          <th>Fecha Atencion</th>
		          <th>Estado</th>
		    </tr>
		    @foreach ($pendientes as $pendiente)
		    @if (RevisionSypelc::find($pendiente->id))
		    	<tr class="success">
		    @else
		    	<tr class="">
		    @endif
		        <td>{{$pendiente->id}}</td> 
		        <td>{{$pendiente->idProyecto}}</td>
		        <td>{{$pendiente->fechaGeneracion}}</td> 

		        @if ($revision = RevisionSypelc::find($pendiente->id))
					<td>NO</td>
					<td>{{$revision->fechaAtencion}}</td>
					<td>{{$revision->estadoVF}}</td>
				@else
					<td>SI</td>
					<td></td>
					<td></td>
		        @endif

		    </tr>
		    @endforeach
		</table>
		{{ $pendientes->links() }}
    </div>	
</div>
@stop