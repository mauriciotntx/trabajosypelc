@extends ('admin/layout')

@section ('title') Importar Archivos @stop

@section ('breadcrumbs')
  {{Breadcrumbs::render(Route::currentRouteName())}}
@stop

@section ('content')
	<h1 class="text-danger">Importar Archivos</h1>
  <div class="panel panel-danger">
    <div class="panel-heading col-md-12"><h2 class="col-md-10">Modulo Importar </h2> <div class="col-md-2"><a href="{{ route('admin.import.resultado.index')}}" class="btn btn-danger">Resultados</a></div></div>
    <div class="panel-body ">
      <div class="col-md-6">
        <h3>Archivos Sypelc</h3>
            <div class="panel panel-default">
            <!-- Default panel contents -->
              <div class="panel-heading">Archivo "Consolidado de Factura"</div>
              <div class="panel-body">
                <p>Archivo de consolidado diario de todas las ejecuciones</p>
                {{ Form::open(array('id' => 'importForm', 'action' => 'Admin_ImportController@postFactura', 'method' => 'POST', 'files' => true))}}
                <div class="form-group">
                  {{ Form::file('archivo') }}
                  {{ Form::button('Importar', array('type' => 'submit', 'class' => 'btn btn-primary disabled')) }}    
                  {{ Form::close() }}
                </div>
              </div>
            </div>

            <div class="panel panel-default">
            <!-- Default panel contents -->
              <div class="panel-heading">Importar Archivo "Devoluciones"</div>
              <div class="panel-body">
                <p>Importa el archivo "devoluciones" en formato .csv dilimitado por ;
                </p>
                {{ Form::open(array('id' => 'importForm', 'action' => 'Admin_ImportController@postDevoluciones', 'method' => 'POST', 'files' => true))}}
                <div class="form-group">
                  {{ Form::file('archivo') }}
                  {{ Form::button('Importar', array('type' => 'submit', 'class' => 'btn btn-primary disabled')) }}    
                  {{ Form::close() }}
                </div>
              </div>
            </div>

            <div class="panel panel-default">
            <!-- Default panel contents -->
              <div class="panel-heading">Importar Archivo "Programacion"</div>
              <div class="panel-body">
                <p>Importa el archivo "programación" en formato .csv dilimitado por ;
                </p>
                {{ Form::open(array('id' => 'importForm', 'action' => 'Admin_ImportController@postProgramacion', 'method' => 'POST', 'files' => true))}}
                <div class="form-group">
                  {{ Form::file('archivo') }}
                  {{ Form::button('Importar', array('type' => 'submit', 'class' => 'btn btn-primary disabled')) }}    
                  {{ Form::close() }}
                </div>
              </div>
            </div>
          </div>
      <div class="col-md-6">
        <h3>Archivos EMSA</h3>
            <div class="panel panel-default">
              <!-- Default panel contents -->
              <div class="panel-heading">Importar Archivo "Solicitudes EMSA"</div>
              <div class="panel-body">
                <p>Importa el archivo "solicitudes" en formato .csv dilimitado por ;
                </p>
                {{ Form::open(array('id' => 'importForm', 'action' => 'Admin_ImportController@postSolicitudes', 'method' => 'POST', 'files' => true))}}
                <div class="form-group">
                  {{ Form::file('archivo') }}
                  {{ Form::button('Importar', array('type' => 'submit', 'class' => 'btn btn-primary disabled')) }}    
                  {{ Form::close() }}
                </div>
              </div>
            </div>

            <div class="panel panel-default">
              <!-- Default panel contents -->
              <div class="panel-heading">Importar Archivo "Revisiones de EMSA"</div>
              <div class="panel-body">
                <p>Importa el archivo "revisiones" en formato .csv dilimitado por ;
                </p>
                {{ Form::open(array('id' => 'importForm', 'action' => 'Admin_ImportController@postRevisiones', 'method' => 'POST', 'files' => true))}}
                <div class="form-group">
                  {{ Form::file('archivo') }}
                  {{ Form::button('Importar', array('type' => 'submit', 'class' => 'btn btn-primary disabled')) }}    
                  {{ Form::close() }}
                </div>
              </div>
            </div>
          </div> 
        </div>
    </div>
  </div>
@stop
