@extends ('admin/layout')

@section ('title') Lista de {{ $nombreModelos }} @stop

@section ('content')

    <h1>Lista de {{ $nombreModelos }}</h1>

    <p><a href="{{ route('admin.'.$nombreModelos.'.create')}}" class="btn btn-primary">Crear Nuevo</a></p>
   	<table class="table table-striped" style="width: 90%">
    <tr>
        @foreach ($nombresAtributos as $nombre)
          <th>{{$nombre}}</th>  
        @endforeach
        <th>Accion</th>
    </tr>
    @foreach ($modelos as $modelo)
    <tr>
        @foreach ($atributos as $atributo)
          <td>{{$modelo->$atributo}}</td> 
        @endforeach
        <td>
          <a href="{{ route('admin.'.$nombreModelos.'.edit', $modelo->id) }}" class="btn btn-primary">
            Editar
          </a>
           <a href="#" data-id="{{ $modelo->id }}" class="btn btn-danger btn-delete">
              Eliminar
          </a>
        </td>
    </tr>
    @endforeach
  </table>
  {{ $modelos->links() }}
  {{ Form::open(array('route' => array('admin.'.$nombreModelos.'.destroy', 'USER_ID'), 'method' => 'DELETE', 'role' => 'form', 'id' => 'form-delete')) }}
{{ Form::close() }}
@stop