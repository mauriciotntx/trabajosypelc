@extends ('admin/layout')

@section ('title') {{ $action.' '.$nombreModelos }} @stop

@section ('content')

  <h1>{{ $action.' '.$nombreModelos }} </h1>

  <p><a href="{{ route('admin.'.$nombreModelos.'.index') }}" class="btn btn-info">Ver Lista de {{$nombreModelos}}</a></p>
  @if ($action == 'Editar')  
    {{ Form::model($modelo, array('route' => array('admin.'.$nombreModelos.'.destroy', $modelo->id), 'method' => 'DELETE', 'role' => 'form')) }}
    <div class="row">
      <div class="form-group col-md-4">
          {{ Form::submit('Eliminar '.$nombreModelos, array('class' => 'btn btn-danger')) }}
      </div>
    </div>
    {{ Form::close() }}
  @endif

{{ Form::model($modelo, $form_data, array('role' => 'form')) }}

  @include ('admin/errors', array('errors' => $errors))

  @include('admin/'.$nombreModelos.'/form' , array($proyectosSelect, $modelo))

  {{ Form::button($action , array('type' => 'submit', 'class' => 'btn btn-primary')) }}    
  
{{ Form::close() }}

@stop