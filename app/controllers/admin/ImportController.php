<?php

class Admin_ImportController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
      	return View::make('admin/import/form');
	}

	public function __construct() {
   		$this->beforeFilter('csrf', array('on'=>'post'));
   		$this->beforeFilter('auth', array('only' => array('postFactura', 'postSolicitudes', 'getIndex', 'postProgramacion', 'postDevoluciones')));
	}

	public function getConsulta(){
		return View::make('admin/import/respuesta');
	}

	public function postTecnicos(){
		if($file = Input::file('archivo')){
			echo 'encontro';
	       	$destinationPath ='/var/www/campanas/public/uploads';
	        $filename = str_random(8).''.$file->getClientOriginalName();
	        $uploadSuccess = Input::file('archivo')->move($destinationPath, $filename);
	        if( $uploadSuccess ){
	        	echo 'subio';
	        	$data = SimpleCsv::import($destinationPath.'/'.$filename);
	        	if($data){
	        		for($i=1; $i<=count($data);$i++){
	        			$valor = new Tecnico;
	        			$valor->id = $data[$i][2];
	        			$valor->nombre = $data[$i][0];
	        			$valor->save();
	        		}	
	        	}
	        }	
		}
	}

	public function  getCombinaciones(){
		$data = SimpleCsv::import('/var/www/campanas/public/uploads/combinaciones.csv');
		if($data){
			echo "subio";
	        for($i=1; $i<count($data);$i++){
	  
	        	$tipo = Tipo::find($data[$i][0]);
	        	if(!$tipo){
	        		$tipo = new Tipo;
	        		$tipo->crear($data[$i][0], str_replace('?', 'N', utf8_decode($data[$i][1])));
	        		$tipo->save();
	        	}

	        	$dependencia = Dependencia::find($data[$i][3]);
	        	if(!$dependencia){
	        		$dependencia = new Dependencia;
	        		$dependencia->id = $data[$i][3];
	        		$dependencia->save();
	        	}

	        	$accion = Accion::find($data[$i][4]);
	        	if(!$accion){
	        		$accion = new Accion;
	        		$accion->crear($data[$i][4], str_replace('?', 'N', utf8_decode($data[$i][5])));
	        		$accion->save();
	        	}

	        	$recomendacion = Recomendacion::find($data[$i][6]);
	        	if(!$recomendacion){
	        		$recomendacion = new Recomendacion;
	        		$recomendacion->crear($data[$i][6], str_replace('?', 'N', utf8_decode($data[$i][7])));
	        		$recomendacion->save();
	        	}

	        	$tipoPQR = TipoPQR::buscar($tipo, $dependencia, $accion);
	        	if(!$tipoPQR){
	        		$tipoPQR = new TipoPQR;
	        		$tipoPQR->crear($tipo, $dependencia, $accion, $data[$i][2]);
	        		$tipoPQR->save();
	        	}

	        	$tipoPQR_has_recomendacion = TipopqrRecomendacion::buscar($tipoPQR, $recomendacion);
	        	if(!$tipoPQR_has_recomendacion){
	        		$tipoPQR_has_recomendacion = new TipopqrRecomendacion;
	        		$tipoPQR_has_recomendacion->crear($tipoPQR, $recomendacion);
	        		$tipoPQR_has_recomendacion->save();
	        	}
	        
	        }	
	    }
	    echo "imprimio";
	}

	public function  getMunicipios(){
		$data = SimpleCsv::import('/var/www/campanas/public/uploads/municipios.csv');
		if($data){
			echo "subio";
	        for($i=0; $i<count($data);$i++){
	        	$valor = new Municipio;
	        	$valor->id = $data[$i][0];
	        	$valor->nombre = $data[$i][1];
	        	$valor->factorDistancia = $data[$i][2];
	        	$valor->save();
	        }	
	    }
	    echo "imprimio";
	}

	public function postValor(){
		echo 'Llego';
		if($file = Input::file('archivo')){
			echo 'encontro';
	       	$destinationPath ='/var/www/campanas/public/uploads';
	        $filename = str_random(8).''.$file->getClientOriginalName();
	        $uploadSuccess = Input::file('archivo')->move($destinationPath, $filename);
	        if( $uploadSuccess ){
	        	echo 'subio';
	        	$data = SimpleCsv::import($destinationPath.'/'.$filename);
	        	if($data){
	        		for($i=0; $i<count($data);$i++){
	        			$valor = new Valor;
	        			$valor->id = $data[$i][0];
	        			$valor->ubicacionRU = $data[$i][1];
	        			$valor->valorUnitario = $data[$i][2];
	        			$valor->puntos = $data[$i][3];
	        			$valor->save();
	        		}	
	        	}
	        }	
		}
	}

	public function postIrregularidad(){
		echo 'entro';
		for($i=1; $i<200;$i++){
			$irregularidad = new Irregularidad;
			$irregularidad->id = $i;
			$irregularidad->save();
		}
	}

	public function postAutogestiones(){

	}

	public function postSuspenciones(){

	}

	public function postFactura(){
		$file = Input::file('archivo');
		$respuesta = Importer::archivo($file, 'fact');
	   	return View::make('admin/import/respuesta', compact('respuesta'));	
	}	

	public function postRevisiones(){
		$file = Input::file('archivo');
		$respuesta = Importer::archivo($file, 'rev');
	   	return View::make('admin/import/respuesta', compact('respuesta'));	
	}

	public function postSolicitudes(){
		$file = Input::file('archivo');
		$respuesta = Importer::archivo($file, 'sol');
	   	return View::make('admin/import/respuesta', compact('respuesta'));	
	}

	public function postProgramacion(){
		$file = Input::file('archivo');
		$fechareal= Importer::obtenerFechaArchivo($file->getClientOriginalName());

		$respuesta = Importer::archivo($file, 'prog', array('fecha' => $fechareal));
	   	return View::make('admin/import/respuesta', compact('respuesta'));			
	}

	public function postDevolucionsistemas(){
		if($file = Input::file('archivo')){ 
			if($ubicacion = Importer::subirArchivo($file)){ // Sube el archivo al servidor
				ini_set('auto_detect_line_endings', true);
	            if (is_file($ubicacion) AND is_readable($ubicacion)) {
	                if (($handle = fopen($ubicacion, "r")) !== false) {
	                	$sypelc = Entidad::find(1);
	        			$ejecutada = Estado::find(3);
	                    while (($line = fgetcsv($handle, null, ';')) !== false) {
	                    	if(is_numeric($data[$i][0]) && $data[$i][2]){
			        			$ejecucion = null;
			        			if($orden = Orden::find($data[$i][0])){
					        		if($estadoOrden = EstadoOrden::buscar($orden, $data[$i][2], $ejecutada, $sypelc)){
					        			$ejecucion = Ejecucion::buscar($estadoOrden);
					        		}
					        	}
					       		else{
					       			$ejecuion = Ejecucion::buscarComoSea($data[$i][2], $data[$i][1], $sypelc);
					       		}	
					       		if($ejecucion){
					       			echo " ejecucion ";
					        		$lugar = LugarDevolucion::find(2);
					        		$devolucion = new Devolucion;
					        		$devolucion->crear($ejecucion, $lugar, $data[$i][2], $data[$i][3], 'N', null);
					        	}
			        		}	
	                    }
	                }
	            }
	        }
	   	}
	}

	public function postDevoluciones(){
		$file = Input::file('archivo');
		$respuesta = Importer::archivo($file, 'dev');
	   	return View::make('admin/import/respuesta', compact('respuesta'));		
	}	


/*public function getProgramaciones(){
		echo "programaciones ";
		$contador= 0;
		$mes=12;
		$year=2013;
		$destinationPath ='/var/www/campanas/public/uploads/programacion';
		$data = null;
		for($dia=1; $dia<8; $dia++){
			if($dia < 10){
				$data = SimpleCsv::import($destinationPath.'/0'.$dia.$mes.$year.'.csv');	
			}
			else{
				$data = SimpleCsv::import($destinationPath.'/'.$dia.$mes.$year.'.csv');	
			}
	        if($data){
	        	for($i=1; $i<count($data);$i++){
	        		if($data[$i][0]){
	        			$contador++;
			        	$fechaReal = $dia.'/'.$mes.'/'.$year;
		        		if ($orden = Orden::find($data[$i][0])) { // Busca si ya existe la orden
		        			$estadoOrden = EstadoOrden::buscar($orden->id, $fechaReal, '2');
			        		if($estadoOrden){ //Busca si ya existe el estado pendiente en esta orden
			        			$estadoOrden->actualizarTecnico($data[$i][16]);	
			        		}
			        		else{
			        			$tecnico = Tecnico::buscarPorNick($data[$i][9]);
			        			$estadoOrden = new EstadoOrden;
			        			$estadoOrden->programar($orden->id, $fechaReal, 2, 1, $tecnico->id); //Crea el estado pendiente para esta orden
			        		}
		        		}
		        		else{
		        			$cliente = Cliente::find($data[$i][1]); //Busca si ya existe el cliente	
		        			if(!$cliente){ //Si no existe el cliente
			        			$municipio = Municipio::buscarPorNombre($data[$i][2]); 
			        			$servicio = Servicio::buscarPorNombre($data[$i][5]);
			        			$cliente = new Cliente; //Crea el cliente
			        			$cliente->crearND($data[$i][1], $municipio->id, $servicio->id, $data[$i][6], $data[$i][3],  $data[$i][4]);
			        		}
			        		$orden = new Orden;
			        		$orden->crearRevision($data[$i][0], $cliente->id, $data[$i][8]);

			        		$tecnico = Tecnico::buscarPorNick($data[$i][9]);
			        		$estadoOrden = new EstadoOrden;
			        		$estadoOrden->programar($orden->id, $fechaReal, 2, 1, $tecnico->id); //Crea el estado pendiente para esta orden
		        		}
		        	}
		        }
		        echo "  contador: " .$contador;
		    }
		}
	}*/

}