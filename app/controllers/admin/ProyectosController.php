<?php

class Admin_ProyectosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$modelos = Proyecto::paginate(10);
		$nombreModelos = 'proyectos';
		$atributos = array('id','nombre');
        $nombresAtributos = array('Id','Nombre');
        return View::make('admin/layoutlist', compact('modelos', 'nombreModelos', 'atributos', 'nombresAtributos'));	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// Creamos un nuevo objeto User para ser usado por el helper Form::model
        $modelo = new Proyecto;
        $form_data = array('route' => 'admin.proyectos.store', 'method' => 'POST');
        $action    = 'Crear';  
        $estadoid ='';
        $nombreModelos = 'proyectos';
        $proyectosSelect = null;	
      	return View::make('admin/layoutform', compact('modelo', 'form_data', 'nombreModelos', 'action', 'estadoid', 'proyectosSelect'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $proyecto = new Proyecto;
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        $proyecto->fill($data);
        $proyecto->save();
        return Redirect::route('admin.proyectos.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$modelo = Proyecto::find($id);
		if (is_null ($modelo)){
			App::abort(404);
		}

		$form_data = array('route' => array('admin.proyectos.update', $modelo->id), 'method' => 'PATCH');
        $action    = 'Editar';
        $estadoid  = 'disabled';
        $nombreModelos = 'proyectos';
        $proyectosSelect = null;
        return View::make('admin/layoutform', compact('modelo', 'form_data', 'nombreModelos', 'action', 'estadoid', 'proyectosSelect'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $proyecto = Proyecto::find($id);
        
        // Si el usuario no existe entonces lanzamos un error 404 :(
        if (is_null ($proyecto))
        {
            App::abort(404);
        }
        
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        $proyecto->nombre = $data['nombre'];
        $proyecto->save();
    	return Redirect::route('admin.proyectos.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$proyecto = Proyecto::find($id);
        
        if (is_null ($proyecto))
        {
            App::abort(404);
        }
        
        $proyecto->delete();

        if (Request::ajax())
        {
            return Response::json(array (
                'success' => true,
                'msg'     => 'Proyecto ' . $proyecto->nombre . ' eliminado',
                'id'      => $proyecto->id
            ));
        }
        else
        {
        	return Response::json(array (
                'success' => true,
                'msg'     => 'Proyecto ' . $proyecto->nombre . ' NO eliminado',
                'id'      => $proyecto->id
            ));
            //return Redirect::route('admin.proyectos.index');
        }
	}

	public function import(){
		$modelos = Proyecto::paginate(8);
		$nombreModelos = 'proyectos';
		$atributos = array('id','nombre');
        return View::make('admin/layoutlist', compact('modelos', 'nombreModelos', 'atributos'));	
	}

}