<?php

class Admin_ResultadosimporController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$resultados = ResultadoImportacion::joinArchivo()
			->select('resultadoImportacion.id', 'contadas', 'creadas', 'actualizadas', 'updated_at', 'created_at', 'noSypelc', 'noEncontradas', 'nombre', 'descartadas')
				->orderBy('created_at', 'desc')
					->paginate(12);
		return View::make('admin/import/list', compact('resultados'));  //Muestra los datos
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$resultado = ResultadoImportacion::find($id);
		if (is_null($resultado)) {
            App::abort(404);
        }
        $descartadas = DB::table('descartada')
       		->where('descartada.id', '=', $resultado->id)
       			->join('motivoDescarte', 'motivoDescarte.id', '=', 'motivo_id')
       				->select('motivoDescarte.nombre', 'idOrden', 'veces')
       					->get();


		return View::make('admin/import/see', compact('resultado', 'descartadas'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}