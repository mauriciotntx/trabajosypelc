<?php

class Admin_EstadisticasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function __construct() {
   		$this->beforeFilter('csrf', array('on'=>'post'));
   		$this->beforeFilter('auth', array('only'=>
   			array('getIndex', 'getPendientes', 'postSavefraudes', 'postSavetecnicos', 
   				'postSaveproyectos', 'getFraudes', 'getRecuperacion', 'getRecomendaciones','getDatostecnicos', 'getDatosproyectos')));
	}


	public function getIndex(){
		$ruta = Route::currentRouteName();
		$revision = ResultadoImportacion::where('tipoarchivo', '=', 'rev')->orderBy('created_at', 'desc')->lists('id', 'id');;
		return View::make('admin/estadisticas/inicio', compact('ruta', 'revision'));
	}


	public function getPendientes(){
		$importacion_id = Input::get('importacion');
		
		$atributos = array('cliente_id', 'cliente_nombre', 'cliente_direccion', 'municipio', 'servicio_id', 'ubicacionUR', 'medidor' ,'orden_id','fechageneracion', 'estado');
		$nombresAtributos = array('Cuenta','Nombre', 'Direccion', 'Municipio', 'Servicio', 'U','Medidor', 'Orden','Generacion', 'Estado');
		$nombreEstadistica = 'Ordenes Pendientes';
		$nombreModelo = 'pendientes';

		$titulosTotales = array('Pendientes', 'Directa', 'Semi-Directa', 'Fraudes no cargados', 'Revisar', 'Conforme', 'No conformes', 'Programadas');
		$modeloTotales = (array) DB::table('cantidadpendientes')->where('id', '=', $importacion_id)->first();
		$atributosTotales = array('pendientes', 'directa', 'semidirecta', 'fraudenocargado', 'revisar' ,'conforme', 'noconforme', 'programada');


		$modelos = DB::table('pendientes')
			->join('importacion_has_ordenes', 'importacion_has_ordenes.orden_id', '=', 'pendientes.orden_id')
				->where('importacion_has_ordenes.importacion_id', '=', $importacion_id)
					->orderBy('fechageneracion', 'desc')->paginate();
	 	
    	return View::make('admin/estadisticas/layoutlist', 
    		compact('modelos', 'nombreEstadistica', 'atributos', 'nombresAtributos', 'nombreModelo', 'titulosTotales',
    				'modeloTotales', 'atributosTotales', 'importacion_id'
    	));  // Muestra los datos 
	}

	public function postPendientes(){
		$importacion_id = Input::get('importacion_id');
		$datos = DB::table('pendientes')
			->select('cliente_id', 'cliente_nombre', 'cliente_direccion', 'municipio', 
					'servicio_id', 'ubicacionUR', 'medidor', 'proyecto_id', 'pendientes.orden_id', 
					'fechageneracion', 'estado', 'prog', 'eje', 'dev')
				->join('importacion_has_ordenes', 'importacion_has_ordenes.orden_id', '=', 'pendientes.orden_id')
						->where('importacion_has_ordenes.importacion_id', '=', $importacion_id)
							->get();
		
		$titulos = array('Cuenta','Nombre', 'Direccion', 'Municipio', 'Servicio', 'U','Medidor', 'Proyecto', 'Orden','Generacion', 'Estado');

		SimpleCsv::export($datos, $titulos);	
	}

	public function getFraudes(){
		$fechaInicio = Input::get('fechaInicio');
		$fechaFinal = Input::get('fechaFinal');

		$atributos = array('orden_id', 'fecha', 'proyecto_id');
		$nombresAtributos = array('Orden', 'Fecha Atencion',  'Proyecto');
		$nombreEstadistica = 'Datos por Orden';
		$nombreModelo = 'fraudes';

		$ordenes = EstadoOrden::joinEjecucion()
			->joinOrden(1)
				->conEntidad(1)
					->conEstado(3)
						->entreFecha($fechaInicio, $fechaFinal);

		$cantOrdenes = $ordenes->count();
		$fraudes = $ordenes->where('ejecucion.estadoFV', '=', 'F');

		$totales = array( 
			array('nombre' => 'Cantidad fraudes', 'valor' => $fraudes->count()),
			array('nombre' => 'Cantidad Ordenes', 'valor' => $cantOrdenes),
			array('nombre' => '% Fraudes', 'valor' => $cantOrdenes / $fraudes->count())
		);

		$modelos = $fraudes->paginate();

		return View::make('admin/estadisticas/layoutlist', compact('fechaInicio', 'fechaFinal', 'nombreEstadistica', 'nombreModelo', 'atributos', 'nombresAtributos', 'modelos', 'totales'));
	}

	public function postFraudes(){
		$fechaInicio = Input::get('fechaInicio');
		$fechaFinal = Input::get('fechaFinal');

		$titulos = array('Orden', 'Fecha Atencion', 'Proyecto');

		$datos = EstadoOrden::joinEjecucion()
			->joinOrden(1)
				->conEntidad(1)
					->conEstado(3)
						->entreFecha($fechaInicio, $fechaFinal)
							->where('ejecucion.estadoFV', '=', 'F')
								->select('orden_id', 'fecha', 'proyecto_id')
									->get();

		SimpleCsv::export($datos, $titulos, 'enloquent');
	}

	public function getOrdenes(){
		$fechaInicio = Input::get('fechaInicio');
		$fechaFinal = Input::get('fechaFinal');

		$atributos = array('orden_id', 'fecha', 'cliente_nombre', 'produccion', 'recuperacion');
		$nombresAtributos = array('Orden', 'Fecha Atencion', 'Cliente', 'Produccion', 'Recuperacion');
		$nombreEstadistica = 'Datos por Orden';
		$nombreModelo = 'ordenes';

		$recuperacion = DB::table('ejecuciones')->select(DB::raw('sum(recuperacion) as recuperacion'))->where('fecha', '>', $fechaInicio)->where('fecha', '<', $fechaFinal)->first();

		$totales = array( 
			array('nombre' => 'Recupearcion', 'valor' => $recuperacion->recuperacion)
		);

		$modelos = DB::table('ejecuciones')
			->where('fecha', '>', $fechaInicio)
				->where('fecha', '<', $fechaFinal)
						->orderBy('fecha', 'desc')
							->paginate(12);
		
		return View::make('admin/estadisticas/layoutlist', compact('fechaInicio', 'fechaFinal', 'nombreEstadistica', 'nombreModelo', 'atributos', 'nombresAtributos', 'modelos', 'totales'));
	}

	public function postOrdenes(){
		$fechaInicio = Input::get('fechaInicio');
		$fechaFinal = Input::get('fechaFinal');

		$datos= DB::table('ejecuciones')
			->where('fecha', '>', $fechaInicio)
				->where('fecha', '<', $fechaFinal)
					->orderBy('recuperacion', 'asc')
						->select('orden_id', 'fecha', 'cliente_nombre', 'produccion', 'recuperacion')
							->get();

		$titulos = array('Orden', 'Fecha Atencion', 'Cliente', 'Produccion', 'Recuperacion');

		SimpleCsv::export($datos, $titulos);	
	}

	public function getTecnicos(){
		$fechaInicio = Input::get('fechaInicio');
		$fechaFinal = Input::get('fechaFinal');

		$atributos = array('tecnico_id', 'tecnico_nombre', 'ejecutadas', 'produccion', 'recuperacion');
		$nombresAtributos = array('Cedula', 'Tecnico', 'Ejecuciones', 'Produccion', 'Recuperacion');
		$nombreEstadistica = 'Datos por Tecnico';
		$nombreModelo = 'tecnicos';

		$ejecuciones = DB::table('ejecuciones')
			->where('fecha', '>', $fechaInicio)
				->where('fecha', '<', $fechaFinal);

		$totales = array( array('nombre' => 'Numero Ejecuciones', 'valor' => $ejecuciones->count()) );

		$modelos = $ejecuciones
			->select(DB::raw("count(*) as ejecutadas , sum(produccion) as produccion  ,  sum(recuperacion) as recuperacion, tecnico_id, tecnico_nombre"))
				->groupBy('tecnico_id', 'tecnico_nombre')
					->orderBy('ejecutadas', 'desc')
						->paginate(12);

		return View::make('admin/estadisticas/layoutlist', compact('fechaInicio', 'fechaFinal', 'nombreEstadistica', 'nombreModelo', 'atributos', 'nombresAtributos', 'modelos', 'totales'));
	}

	public function postTecnicos(){
		$fechaInicio = Input::get('fechaInicio');
		$fechaFinal = Input::get('fechaFinal');

		$datos  = DB::table('ejecuciones')
			->where('fecha', '>', $fechaInicio)
				->where('fecha', '<', $fechaFinal)
					->select(DB::raw('tecnico_id, tecnico_nombre, count(*) as ejecutadas , sum(produccion) as produccion , sum(recuperacion) as recuperacion'))
						->groupBy('tecnico_id', 'tecnico_nombre')
							->orderBy('ejecutadas', 'desc')
								->get();

		$titulos = array('Cedula', 'Tecnico', 'Ejecuciones', 'Produccion', 'Recuperacion');
		SimpleCsv::export($datos, $titulos);	
	}

	public function getProyectos(){
		$fechaInicio = Input::get('fechaInicio');
		$fechaFinal = Input::get('fechaFinal');
		$page = Input::get('page', 1);

		$resultado = DB::select(DB::raw("select * from tablaestados('".$fechaInicio."','".$fechaFinal."')"));
		$pages = array_chunk($resultado, 12);
		$datosporproyecto = Paginator::make($pages[$page -1], count($resultado), 12);

		return View::make('admin/estadisticas/proyectos', compact('fechaInicio', 'fechaFinal', 'datosporproyecto'));
	}

	public function postProyectos(){
		$fechaInicio = Input::get('fechaInicio');
		$fechaFinal = Input::get('fechaFinal');

		$titulos = array('Proyecto', 'Asignadas', 'Ejecutadas Sypelc', 'Ejecutadas Emsa', 'Produccion', 'Recuperacion');

		$datos = DB::select(DB::raw("select * from tablaestados('".$fechaInicio."','".$fechaFinal."')"));

		SimpleCsv::export($datos, $titulos);	
	}
}