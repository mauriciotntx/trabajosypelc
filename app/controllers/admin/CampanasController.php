<?php

class Admin_CampanasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index()
	{
		$modelos = Campana::paginate(10);
		$nombreModelos = 'campanas';
		$atributos = array('id','nombre', 'idProyecto');
        $nombresAtributos = array('Id','Nombre', 'Proyecto');
        return View::make('admin/layoutlist', compact('modelos', 'nombreModelos', 'atributos', 'nombresAtributos'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// Creamos un nuevo objeto User para ser usado por el helper Form::model
        $modelo = new Campana;
        $form_data = array('route' => 'admin.campanas.store', 'method' => 'POST');
        $action    = 'Crear';  
        $estadoid ='';
        $nombreModelos = 'campanas';	
        //$proyectos = Proyecto::all(array('nombre', 'id'));
        $proyectos = Proyecto::all(array('id','nombre'));
        $proyectosSelect = array();
        foreach ($proyectos as $proyecto) {
        	$proyectosSelect[$proyecto->id]=$proyecto->nombre;
        }
      	return View::make('admin/layoutform', compact('modelo', 'form_data', 'nombreModelos', 'action', 'estadoid', 'proyectosSelect'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $campana = new Campana;
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        $campana->fill($data);
        $campana->save();
        return Redirect::route('admin.campanas.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$modelo = Campana::find($id);
		if (is_null ($modelo)){
			App::abort(404);
		}

		$form_data = array('route' => array('admin.campanas.update', $modelo->id), 'method' => 'PATCH');
        $action    = 'Editar';
        $estadoid  = 'disabled';
        $nombreModelos = 'campanas';
        $proyectos = Proyecto::all(array('id','nombre'));
        $proyectosSelect = array();
        foreach ($proyectos as $proyecto) {
        	$proyectosSelect[$proyecto->id]=$proyecto->nombre;
        }
        return View::make('admin/layoutform', compact('modelo', 'form_data', 'nombreModelos', 'action', 'estadoid', 'proyectosSelect'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $campana = Campana::find($id);
        
        // Si el usuario no existe entonces lanzamos un error 404 :(
        if (is_null ($campana))
        {
            App::abort(404);
        }
        
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        $campana->nombre = $data['nombre'];
        $campana->idProyecto = $data['idProyecto'];
        $campana->save();
    	return Redirect::route('admin.campanas.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$campana = Campana::find($id);
        
        if (is_null ($campana))
        {
            App::abort(404);
        }
        
        $campana->delete();

        if (Request::ajax())
        {
            return Response::json(array (
                'success' => true,
                'msg'     => 'Campana ' . $campana->nombre . ' eliminado',
                'id'      => $campana->id
            ));
        }
        else
        {
            return Redirect::route('admin.campanas.index');
        }
	}

}