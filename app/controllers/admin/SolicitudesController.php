<?php

class Admin_SolicitudesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$modelos  = Orden::where('tipoOrden_id', '=', '2')
			->paginate(12);
		$nombreModelos = 'solicitudes';
		$atributos = array('ordenPQR_tipoPQR_dependencia_id','ordenPQR_solicitud', 'ordenPQR_consecutivo', 'cliente_id');
		$nombresAtributos = array('Dependencia','Solicitud', 'Consecutivo', 'Cliente');
        return View::make('admin/layoutlist', compact('modelos', 'nombreModelos', 'atributos', 'nombresAtributos'));	
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}