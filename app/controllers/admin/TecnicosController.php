<?php

class Admin_TecnicosController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */


	public function index()
	{
		$modelos = Tecnico::paginate(8);
		$nombreModelos = 'tecnicos';
		$atributos = array('id','nombre', 'nick');
		$nombresAtributos = array('Id','Nombre', 'Nick');
        return View::make('admin/layoutlist', compact('modelos', 'nombreModelos', 'atributos', 'nombresAtributos'));		
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		// Creamos un nuevo objeto User para ser usado por el helper Form::model
        $modelo = new Tecnico;
        $form_data = array('route' => 'admin.tecnicos.store', 'method' => 'POST');
        $action    = 'Crear';  
        $estadoid ='';
        $nombreModelos = 'tecnicos';	
        $proyectosSelect = null;
        return View::make('admin/layoutform', compact('modelo', 'form_data', 'nombreModelos', 'action', 'estadoid', 'proyectosSelect'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $tecnico = new Tecnico;
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        $tecnico->fill($data);
        $tecnico->save();
        return Redirect::route('admin.tecnicos.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$modelo = Tecnico::find($id);
		if (is_null ($modelo)){
			App::abort(404);
		}

		$form_data = array('route' => array('admin.tecnicos.update', $modelo->id), 'method' => 'PATCH');
        $action    = 'Editar';
        $estadoid  = 'disabled';
        $nombreModelos = 'tecnicos';
$proyectosSelect = null;
        return View::make('admin/layoutform', compact('modelo', 'form_data', 'nombreModelos', 'action', 'estadoid', 'proyectosSelect'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		// Creamos un nuevo objeto para nuestro nuevo usuario
        $tecnico = Tecnico::find($id);
        
        // Si el usuario no existe entonces lanzamos un error 404 :(
        if (is_null ($tecnico))
        {
            App::abort(404);
        }
        
        // Obtenemos la data enviada por el usuario
        $data = Input::all();
        $tecnico->nombre = $data['nombre'];
        $tecnico->nick = $data['nick'];
        $tecnico->save();
    	return Redirect::route('admin.tecnicos.index');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$tecnico = Tecnico::find($id);
        
        if (is_null ($tecnico))
        {
            App::abort(404);
        }
        
        $tecnico->delete();

        if (Request::ajax())
        {
            return Response::json(array (
                'success' => true,
                'msg'     => 'Tecnico ' . $tecnico->nombre . ' eliminado',
                'id'      => $tecnico->id
            ));
        }
        else
        {
            return Redirect::route('admin.tecnicos.index');
        }
	}

}