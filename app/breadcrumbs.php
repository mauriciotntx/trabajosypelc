<?php

// INICIO
    Breadcrumbs::register('admin', function($breadcrumbs) {
        $breadcrumbs->push('Inicio', route('admin'));
    });

    // MODULO ADMIN
        Breadcrumbs::register('estadisticas', function($breadcrumbs) {
            $breadcrumbs->parent('admin');
            $breadcrumbs->push('Estadisticas', route('estadisticas'));
        });

    // Modulo ESTADISTICAS
        Breadcrumbs::register('estadisticas', function($breadcrumbs) {
            $breadcrumbs->parent('admin');
            $breadcrumbs->push('Estadisticas', route('estadisticas'));
        });

        // Estadisticas/Page
            Breadcrumbs::register('estadisticas/page', function($breadcrumbs, $page) {
                $breadcrumbs->parent('estadisticas');
                $breadcrumbs->push($page, route('estadisticas/'.$page));
            });

    // Modulo IMPORT
        Breadcrumbs::register('import', function($breadcrumbs) {
            $breadcrumbs->parent('admin');
            $breadcrumbs->push('Import', route('import'));
        });

        Breadcrumbs::register('get admin/import', function($breadcrumbs) {
            $breadcrumbs->parent('admin');
            $breadcrumbs->push('Import', route('import'));
        });

        Breadcrumbs::register('admin.import.resultado.index', function($breadcrumbs) {
            $breadcrumbs->parent('import');
            $breadcrumbs->push('Resultado', route('admin.import.resultado.index'));
        });

        Breadcrumbs::register('admin.import.resultado.show', function($breadcrumbs, $id) {
            $breadcrumbs->parent('admin.import.resultado.index');
            $breadcrumbs->push($id, route('admin.import.resultado.show', $id));
        });

        // Import/Page
            Breadcrumbs::register('import/consulta', function($breadcrumbs) {
                $breadcrumbs->parent('import');
                $breadcrumbs->push('Consulta', route('import/consulta'));
            });

            Breadcrumbs::register('import/factura', function($breadcrumbs) {
                $breadcrumbs->parent('import');
                $breadcrumbs->push('Factura', route('import/factura'));
            });

            Breadcrumbs::register('import/revisiones', function($breadcrumbs) {
                $breadcrumbs->parent('import');
                $breadcrumbs->push('Revisiones', route('import/revisiones'));
            });

            Breadcrumbs::register('import/solicitudes', function($breadcrumbs) {
                $breadcrumbs->parent('import');
                $breadcrumbs->push('Solicitudes', route('import/solicitudes'));
            });

            Breadcrumbs::register('import/programacion', function($breadcrumbs) {
                $breadcrumbs->parent('import');
                $breadcrumbs->push('Programación', route('import/programacion'));
            });

            Breadcrumbs::register('import/devoluciones', function($breadcrumbs) {
                $breadcrumbs->parent('import');
                $breadcrumbs->push('Devoluciones', route('import/devoluciones'));
            });