<?php


class Orden extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'orden';
	protected $fillable = array('id', 'cliente_id', 'tipoOrden_id','proyecto_id', 'ordenPQR_solicitud', 'ordenPQR_consecutivo', 'ordenPQR__tipoPQR_dependencia_id');
	public $timestamps = true;
	public $incrementing = false; 
	public $errors;

	public function crearRevision($id, $cliente_id, $proyecto_id){
		$this->id = $id;
		$this->cliente_id = $cliente_id;
		$this->proyecto_id = $proyecto_id;
		$this->tipoOrden_id = 1;
	}

	public function crearSolicitud($cliente_id, $ordenPQR){
		$this->id = $ordenPQR->tipoPQR_dependencia_id.$ordenPQR->solicitud.$ordenPQR->consecutivo;
		$this->cliente_id = $cliente_id;
		$this->ordenPQR_tipoPQR_dependencia_id = $ordenPQR->tipoPQR_dependencia_id;
		$this->ordenPQR_solicitud = $ordenPQR->solicitud;
		$this->ordenPQR_consecutivo = $ordenPQR->consecutivo;
		$this->tipoOrden_id = 2;
	}

	public static function buscarPqr($solicitud, $consecutivo){
		$resultados = Orden::where('ordenPQR_solicitud', '=', $solicitud)
			->where('ordenPQR_consecutivo', '=', $consecutivo)	
				->count();
		if($resultados == 1){
			$orden = Orden::where('ordenPQR_solicitud', '=', $solicitud)
				->where('ordenPQR_consecutivo', '=', $consecutivo)	
					->first();
			return $orden;
		}
		return null;
	}

	public static function actualizarProyecto($orden, $proyecto){
		Orden::where('id', '=', $orden->cliente_id) //Actualiza la base de datos
			->update(array('proyecto_id' => $proyecto->id));
	}

	public static function findSelectId($id){ 
		return DB::table('orden')->select ('id')->where('id', '=' ,$id)->first();
	}
}