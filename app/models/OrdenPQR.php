<?php


class OrdenPQR extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ordenPQR';
	protected $fillable = array('solicitud', 'consecutivo', 'tipoPQR_dependencia_id', 'tipoPQR_tipo_id', 'tipoPQR_accion_id', 'direccion', 'observacion');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($solicitud, $consecutivo, $tipoPQR){
	    $this->solicitud = $solicitud;
	    $this->consecutivo = $consecutivo;
	    $this->tipoPQR_dependencia_id = $tipoPQR->dependencia_id;
	    $this->tipoPQR_tipo_id = $tipoPQR->tipo_id;
	    $this->tipoPQR_accion_id = $tipoPQR->accion_id;
	}

	public function agregarDireccionObservacion($direccion, $observacion){
		$this->direccion = $direccion;
	    $this->observacion = $observacion;
	}

	public static function buscar($solicitud, $consecutivo, $tipoPQR){
		$ordenPqr = OrdenPQR::where('solicitud', '=', $solicitud)
			->where('consecutivo', '=', $consecutivo)	
				->where('tipoPQR_dependencia_id', '=', $tipoPQR->dependencia_id)
					->first();
		return $ordenPqr;
	}

}






