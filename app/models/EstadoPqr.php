<?php

class EstadoPqr extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'estadoPqr';
	protected $fillable = array('id', 'tipoSA', 'nombre', 'descripcion');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;


	public static function buscar($id, $tipoSA){
		$estadoPqr = EstadoPqr::where('id', '=', $id)	
			->where('tipoSA', '=', $tipoSA)	
				->first();
		return $estadoPqr;
	}
}