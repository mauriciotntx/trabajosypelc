<?php


class RevisionEMSA extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'revisionEMSA';
	protected $fillable = array('id', 'numeroActa', 'fechaRealizacion','estado','fechaGrabacion','obs_cancela','fechaRevision');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($idR, $numeroActaR, $fechaRealizacionR, $estadoR, $fechaGrabacionR, $fechaRevisionR){
		$this->id = $idR;
		if($numeroActaR){
			$this->numeroActa = $numeroActaR;
		}
		if($fechaRealizacionR){
			$this->fechaRealizacion = $fechaRealizacionR;
		}
		if($fechaGrabacionR){
			$this->fechaGrabacion = $fechaGrabacionR;
		}
		if($fechaRevisionR){
			$this->fechaRevision = $fechaRevisionR;	
		}
		$this->estado=$estadoR;
		$this->save();
	}

	public function actualizar($numeroActaR, $fechaRealizacionR, $estadoR, $fechaGrabacionR, $fechaRevisionR){
		if($numeroActaR){
			$this->numeroActa = $numeroActaR;
		}
		if($fechaRealizacionR){
			$this->fechaRealizacion = $fechaRealizacionR;
		}
		if($fechaGrabacionR){
			$this->fechaGrabacion = $fechaGrabacionR;
		}
		if($fechaRevisionR){
			$this->fechaRevision = $fechaRevisionR;	
		}
		
		$this->estado=$estadoR;
		$this->save();
	}

	public function setPendiente($idR, $estadoR){
		$this->id = $idR;
		$this->estado =$estadoR;
		$this->save();
	}
}