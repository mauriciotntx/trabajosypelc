<?php


class Tecnico extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tecnico';
	protected $fillable = array('id', 'nombre', 'nick');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public static function buscar($id){
		$tecnico = null;
		if(is_numeric($id)){
			$tecnico = Tecnico::find($id);
		}
		else if(strlen($id)>0){
			$tecnico = DB::table('tecnico')
				->where('nick', '=', $id)	
					->first();	
			if(!$tecnico){
				$nombresTecnico = NombresTecnico::find($id);
				if($nombresTecnico){
					$tecnico = Tecnico::find($nombresTecnico->tecnico_id);
				}
			}	
		}
		if($tecnico){
			return $tecnico;
		}
		else{
			return Tecnico::find(0);	
		}
	}
}