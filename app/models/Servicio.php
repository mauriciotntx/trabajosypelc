<?php


class Servicio extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'servicio';
	protected $fillable = array('id', 'nombre');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public static function buscarPorNombre($nombre){
		$servicio = DB::table('servicio')
			->where('nombre', '=', $nombre)
		    	->first();
		if(!$servicio){
			return 'ZZ';	
		}
		return $servicio->id;
	}
}