<?php


class TipoOrden extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tipoOrden';
	protected $fillable = array('id', 'nombre');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public static function buscarPorNombre($nombre){
		$tipoOrden = DB::table('tipoOrden')
			->where('nombre', '=', trim($nombre))
		    	->first();
		return $tipoOrden;
	}

}