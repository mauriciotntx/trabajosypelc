<?php


class NombresTecnico extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'nombresTecnico';
	protected $fillable = array('tecnico_id', 'nombre');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;
}