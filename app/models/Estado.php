<?php

class Estado extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'estado';
	protected $fillable = array('id', 'nombre', 'descripcion');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

}