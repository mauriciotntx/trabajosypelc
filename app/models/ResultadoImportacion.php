<?php


class ResultadoImportacion extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'resultadoImportacion';
	protected $fillable = array('id');
	public $timestamps = true;
	public $incrementing = true; 
	public $errors;

	public function scopeJoinArchivo($query){
		return $query->join('archivo', 'tipoarchivo', '=', 'archivo.id');
	}

}