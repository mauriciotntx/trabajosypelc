<?php


class Accion extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'accion';
	protected $fillable = array('id', 'descripcion');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($id, $descripcion){
		$this->id = $id;
		if($descripcion){
	    	$this->descripcion = $descripcion;
	    }
	}
}