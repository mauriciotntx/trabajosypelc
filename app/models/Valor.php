<?php


class Valor extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	
	protected $table = 'valor';
	protected $fillable = array('id', 'ubicacionRU', 'valorUnitario', 'puntos');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;


	public static function buscar($id, $ubicacionRU){
		$valor = Valor::where('id', '=', $id) //Busca si ya existe el estado pendiente en esta orden
			->where('ubicacionRU', '=', $ubicacionRU)
				->first();
		return $valor;
	}
}