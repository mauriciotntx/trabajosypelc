<?php


class TipopqrRecomendacion extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tipoPQR_has_recomendacion';
	protected $fillable = array('tipoPQR_tipo_id', 'tipoPQR_dependencia_id', 'tipoPQR_accion_id', 'recomendacion_id');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($tipoPQR, $recomendacion){
	    $this->tipoPQR_dependencia_id = $tipoPQR->dependencia_id;
	    $this->tipoPQR_tipo_id = $tipoPQR->tipo_id;
	    $this->tipoPQR_accion_id = $tipoPQR->accion_id;
	    $this->recomendacion_id = $recomendacion->id;  
	}

	public static function buscar($tipoPQR, $recomendacion){
		$ordenPQR_has_recomendacion = DB::table('tipoPQR_has_recomendacion') //Busca si el medidor ya está asociado al cliente
			->where('tipoPQR_tipo_id' , '=' , $tipoPQR->tipo_id)
				->where('tipoPQR_dependencia_id', '=' , $tipoPQR->dependencia_id)
					->where('tipoPQR_accion_id', '=' , $tipoPQR->accion_id)
						->where('recomendacion_id', '=' , $recomendacion->id)
							->first();
		return $ordenPQR_has_recomendacion;
   	}
}