<?php


class ValoresRevision extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	
	protected $table = 'valoresRevision';
	//protected $fillable = array('id', 'ubicacionUR', 'valorUnitario', 'puntos');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($idRevisionR, $idValorR, $ubicacionRUR){
		$this->idRevision = $idRevisionR;
		$this->idValor = $idValorR;
		$this->ubicacionRU = $ubicacionRUR;
		$this->save();
	}
}