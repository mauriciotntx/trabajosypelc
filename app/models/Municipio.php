<?php


class Municipio extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'municipio';
	protected $fillable = array('id', 'nombre', 'factorDistancia');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($data){
		$this->id = $data[0]; 
	    $this->nombre = $data[1];
	    $this->factorDistancia = $data[2];
	    $this->save();
	}

	public static function buscarPorNombre($nombre){
		$municipio = DB::table('municipio')
			->where('nombre', '=', $nombre)
		    	->first();
		if(!$municipio){
			$municipio = Municipio::find(0);	
		}
		return $municipio;
	}

	public static function buscarPorIdEmnsa($idEmsa){
		$municipio = null;
		if(is_numeric($idEmsa)){
			$municipio = DB::table('municipio')
				->where('idEmsa', '=', $idEmsa)
			    	->first();	
		}
		if(!$municipio){
			return 0;	
		}
		return $municipio->id;
	}
}