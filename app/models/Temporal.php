<?php


class Temporal extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'temporal';
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;
}