<?php


class ClienteMedidor extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cliente_has_medidor';
	protected $fillable = array('clinete_id', 'meidor_marca', 'medidor_serie');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;


	public function crear($medidor, $cliente_id){
		$this->cliente_id = $cliente_id;
		$this->medidor_serie = $medidor->serie;
		$this->medidor_marca = $medidor->marca;
		$this->save();
	}

	public static function buscar($medidor, $cliente_id){
		$cliente_has_medidor = DB::table('cliente_has_medidor') //Busca si el medidor ya está asociado al cliente
			->where('medidor_marca' , '=' , $medidor->marca)
				->where('medidor_serie', '=' , $medidor->serie)
					->where('cliente_id', '=' , $cliente_id)
						->first();
		return $cliente_has_medidor;
   	}
}