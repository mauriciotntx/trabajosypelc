<?php


class IrregularidadesRevision extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	
	protected $table = 'irregularidadesRevision';
	protected $fillable = array('idRevision', 'idIrregularidad');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($idIrregularidadR, $idRevisionR){
		$this->idRevision = $idRevisionR;
		$this->idIrregularidad = $idIrregularidadR;
		$this->save();
	}
}