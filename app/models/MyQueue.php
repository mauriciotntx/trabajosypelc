<?php 

class MyQueue{

	public static function solicitudes($job, $data){
		$resultado = ResultadoImportacion::find($data['resultado']);	
		$resultado->increment('contadas');

		$numOrden = trim($data['line'][0]).trim($data['line'][1]).trim($data['line'][2]);
	    if( is_numeric($data['line'][3]) && is_numeric($numOrden) && Importer::validarFecha($data['line'][26])){	
			$orden = Orden::find($numOrden);
		    if (!$orden){
				$municipio = Municipio::buscarPorIdEmnsa($data['line'][5]);
			    $servicio = Servicio::buscarPorNombre(trim($data['line'][7]));

				$cliente_id = Importer::clienteEmsa($data['line'][3] ,$municipio, $servicio, $data['line'][4], $data['line'][10], $data['line'][11], $data['line'][8],$data['line'][9], $data['line'][17]);
				$medidor = Importer::medidor($data['line'][16], $data['line'][15], $data['line'][13], $data['line'][14], $data['line'][18]);
				
				if($cliente_id && $medidor){
					$cliente_has_medidor = Importer::cliente_has_medidor($medidor, $cliente_id);
				}

				if(is_numeric($data['line'][20]) && is_numeric($data['line'][0]) && is_numeric($data['line'][22])){
					$tipoPQR = Importer::tipoPQR($data['line'][20], $data['line'][0], $data['line'][22]);
					$ordenPqr = Importer::ordenPqr($data['line'][1], $data['line'][2], $tipoPQR, $data['line'][36], $data['line'][27]);		        		
					$orden = new Orden;
					$orden->crearSolicitud($cliente_id, $ordenPqr);
					$orden->save();
					Importer::estadoOrden($orden->id, $data['line'][26], 1, 2, null);
					$resultado->increment('creadas');
				}

			}
			else{
				$resultado->increment('actualizadas');
			}
			if($orden && Importer::validarFecha($data['line'][35])){
				$estadoEjecucion = Importer::estadoOrden($orden->id, $data['line'][35], 3, 2,null);
				if($estadoEjecucion){
					$ejecucion = Importer::ejecucion($estadoEjecucion, null, $data['line'][34], null);
					$ejecucion_has_estadoPqr = Importer::ejecucion_has_estadoPqr($ejecucion, $data['line'][30], 'S');
					$ejecucion_has_estadoPqr = Importer::ejecucion_has_estadoPqr($ejecucion, $data['line'][31], 'A');

				   	$lecturaMedidor = Importer::lecturaMedidor('E', $data['line'][16], $data['line'][15], $data['line'][24], $ejecucion);
					$lecturaMedidor = Importer::lecturaMedidor('I', $data['line'][16], $data['line'][15], $data['line'][25], $ejecucion);
				}
			}	
		}
		else {
			$resultado->increment('descartadas');
			if(!is_numeric($numOrden)){
			 	Importer::descartada($resultado->id, $numOrden, 1);
			}
			else if(!Importer::validarFecha($data['line'][26])){
				Importer::descartada($resultado->id, $numOrden, 3);
			}
			else{
				Importer::descartada($resultado->id, $numOrden, 2);
			}

		}

		$resultado->save();
		$job->delete();
	}
	
	public static function revisiones($job, $data){
			$resultado = ResultadoImportacion::find($data['resultado']);	
			$resultado->increment('contadas');
			
			if(is_numeric($data['line'][1])){
		        $tipoArchivo = 'Revision'; //Archivo sin estados 
				$revisor = 'SYPELC';
			    $numProyecto = 0;
		        $numOrden = $data['line'][1];
		        $fechaGeneracion = $data['line'][6];
		        $numCliente = $data['line'][2];
		        
		    }
		    else{
		        $tipoArchivo = 'REV'; //Archivo con estados de ejecucion y pendientes
			    $numOrden = $data['line'][6];
			    $revisor = trim($data['line'][11]);	
			    $numProyecto = $data['line'][13];
			    $fechaGeneracion = $data['line'][7];
			    $numCliente = $data['line'][0];
			    $medidorarray = explode(" ", $data['line'][4]);
		    }

		    if($revisor=='SYPELC' && is_numeric($numOrden) && Importer::validarFecha($fechaGeneracion) && is_numeric($numCliente) ){
				$orden = Orden::find($numOrden); // Busca si ya existe la orden
				if(!$orden){
				    if($tipoArchivo == 'Revision'){
				        $municipio = Municipio::buscarPorIdEmnsa($data['line'][5]);
		        		$servicio = Servicio::buscarPorNombre(trim($data['line'][7]));
		        					
				        $cliente_id = Importer::clienteEmsa($numCliente,$municipio, $servicio, $data['line'][3], Importer::quitarAcentos($data['line'][10]),Importer::quitarAcentos($data['line'][11]),$data['line'][8], $data['line'][9], $data['line'][17]);
				        $medidor = Importer::medidor($data['line'][16], $data['line'][15], $data['line'][13], $data['line'][14], $data['line'][18]);
				    }
				    else{
				        $municipio = Municipio::buscarPorNombre(trim($data['line'][1]));
				        $servicio = Servicio::find(trim($data['line'][2]));
				        if(!$servicio){
				        	$servicio = Servicio::find('ZZ');
				        }
				        $cliente_id = Importer::clientes($numCliente,$municipio->id, $servicio->id, $data['line'][3]);
				        $medidor = Importer::medidor($medidorarray[1], $medidorarray[0], null, null, null);
				        
				    }
				  	if($cliente_id && $medidor){
						$cliente_has_medidor = Importer::cliente_has_medidor($medidor, $cliente_id);
					}
				        		
				   	$orden = Importer::revision($numOrden, $cliente_id, $numProyecto);
				    $estadoPendiente = Importer::estadoOrden($orden->id, $fechaGeneracion, 1, 2, null);
				    $importacion_has_ordenes = ImportacionOrdenes::crear($orden->id, $resultado->id);
				    $resultado->increment('creadas');
			    }
			    else{
			        $resultado->increment('actualizadas');	
			        $orden->increment('updates');
					$orden->save();	     
					if(!ImportacionOrdenes::buscar($orden->id, $resultado->id)){
						$importacion_has_ordenes = ImportacionOrdenes::crear($orden->id, $resultado->id);	
					}
			    	if($orden->proyecto_id==0 && $numProyecto!=0){
			    		$orden->proyecto_id = $numProyecto;
			    		$orden->save();
			    	}			    		
			    }
			    if($orden && $tipoArchivo == 'REV' && Importer::validarFecha($data['line'][9])){
				    $estadoEjecucion = Importer::estadoOrden($orden->id, $data['line'][8], 3, 2, null);
				    if(is_numeric($data['line'][13]) && $orden->proyecto_id == 0 && $proyecto = Proyecto::find($data['line'][13])){
				      	$orden->proyecto_id = $proyecto->id;
				       	$orden->save();
				    }
				}
			}
			else if($revisor=='SYPELC'){
				$resultado->increment('descartadas');
				if(!is_numeric($numOrden)){
				 	Importer::descartada($resultado->id, $numOrden, 1);
				}
				else if(!Importer::validarFecha($fechaGeneracion)){
					Importer::descartada($resultado->id, $numOrden, 3);
				}
				else {
					Importer::descartada($resultado->id, $numOrden, 2);
				}
			}
			else{
				$resultado->increment('noSypelc');	
			}
			$resultado->save();
			$job->delete();
	}
	
	public static function factura($job, $data){
		$resultado = ResultadoImportacion::find($data['resultado']);	
		$resultado->increment('contadas');
		$ejecutada = 3;
		$sypelc = 1;
		if(is_numeric($data['line'][2]) && Importer::validarFecha($data['line'][5])){
			if($tipoOrden = TipoOrden::buscarPorNombre($data['line'][4])){
				$orden = null;
				if($tipoOrden->id == 1){
					$orden = Orden::find(trim($data['line'][2])); 
				}
				else if($tipoOrden->id == 2 && is_numeric($data['line'][33])){
					$orden = Orden::buscarPqr($data['line'][2],$data['line'][33]);
				}

				if($orden){			
					$estadoEjecucion = Importer::estadoOrden($orden->id, $data['line'][5], $ejecutada, $sypelc, trim(utf8_decode(str_replace('Ñ', 'N', $data['line'][17]))));
					if($estadoEjecucion){
						$resultado->increment('creadas');
						$aforo=0;
						if(is_numeric($data['line'][28])){
							$aforo = $data['line'][28];
						}
						$ejecucion = Importer::ejecucion($estadoEjecucion, $data['line'][7], $data['line'][6], $aforo);
						
						if($data['line'][22] && $data['line'][23]){
							Importer::lecturaMedidor('E', $data['line'][22], $data['line'][23], $data['line'][24], $ejecucion);
						}
						if($data['line'][25] && $data['line'][26]){
							Importer::lecturaMedidor('I', $data['line'][25], $data['line'][26], $data['line'][27], $ejecucion);
						} 
											
						for($irr=8; $irr<14; $irr++){
							if($data['line'][$irr] && is_numeric($data['line'][$irr])){
								if($valor = Valor::buscar($data['line'][$irr],$data['line'][14])){
									if(!ValoresEjecucion::buscar($ejecucion, $valor)){
										$valoresEjecucion = new ValoresEjecucion;
										$valoresEjecucion->crear($ejecucion, $valor);
									}
								}
							}
						}

						for($irr=29; $irr<33; $irr++){
							if($data['line'][$irr] && is_numeric($data['line'][$irr]) && $data['line'][$irr]<200){
								if($irregularidad = Irregularidad::find($data['line'][$irr])){
									if(!IrregularidadesEjecucion::buscar($ejecucion, $irregularidad)){
										$irregularidades = new IrregularidadesEjecucion;
										$irregularidades->crear($ejecucion, $irregularidad);
									}
								}
							}
						}
					}
					else{
						$resultado->increment('actualizadas');
					}
				}
				else{	
					$resultado->increment('noEncontradas');
				}
			}	
			else{
				$resultado->increment('descartadas');	
			}

		}
		else{
			if(!is_numeric($data['line'][2])){
			 	Importer::descartada($resultado->id, $data['line'][2], 1);
			}
			else {
				Importer::descartada($resultado->id, $data['line'][2], 3);
			}
			$resultado->increment('descartadas');		
		}
		$resultado->save();	
		$job->delete();
	}

	public static function devoluciones($job, $data){
		$resultado = ResultadoImportacion::find($data['resultado']);	
		$resultado->increment('contadas');
		$devuelta = 4;
		$sypelc = 1;

		//if(is_numeric($data['line'][3])){ //Archivo de Terreno
			$numOrden=$data['line'][1];
			$consecutivo = $data['line'][2];
			$acta = $data['line'][3];
			$fechaDevolucion = $data['line'][4];
			$motivo = $data['line'][9];
			$oficioApplus = $data['line'][11];
			$fechaCorrecion = $data['line'][12];
			$corregidoSN = $data['line'][13];
			$tipoOrden = $data['line'][14];
			$LugarDevolucion_id = 1;
		//}                                              
		/*else{ //Archivo de Sistemas
			$numOrden=$data['line'][0];
			$acta = $data['line'][1];
			$fechaDevolucion = $data['line'][2];
			$motivo = $data['line'][3];	
			$oficioApplus = $data['line'][4];
			$tipoOrden = null;
			$consecutivo = null;
			$fechaCorrecion = null;
			$corregidoSN = null;
			$LugarDevolucion_id = 2;
		}*/
		if(is_numeric($numOrden) && Importer::validarFecha($fechaDevolucion)){
			$ejecucion = null;
			$orden = null;
    		if ($tipoOrden == 'PQR' && is_numeric($consecutivo)){
    			$orden = Orden::buscarPqr($numOrden,$consecutivo);
    		}
        	else if(is_numeric($numOrden)){
        		$orden = Orden::find($numOrden);
        	}
        	if($orden){
				$estadoDevolucion = Importer::estadoOrden($orden->id, $fechaDevolucion, $devuelta, $sypelc, null);
        		if ($estadoDevolucion) {
        			$resultado->increment('creadas');
	        		$devolucion = new Devolucion;
	        		$devolucion->crear($estadoDevolucion, $LugarDevolucion_id, Importer::quitarAcentos ($motivo),$corregidoSN, $fechaCorrecion);
	        		$devolucion->save();
	        	}
	        	else{
					$resultado->increment('actualizadas');
				}
			}
			else{	
				$resultado->increment('noEncontradas');
			}
		}	
		else{
			if(!is_numeric($numOrden)){
			 	Importer::descartada($resultado->id, $numOrden, 1);
			}
			else {
				Importer::descartada($resultado->id, $numOrden, 3);
			}
			$resultado->increment('descartadas');	
		}
		$resultado->save();	
		$job->delete();
	}

	public static function programacion($job, $data){
		$resultado = ResultadoImportacion::find($data['resultado']);	
		$resultado->increment('contadas');

		$sypelc = 1;
		$programada = 2;
		if(is_numeric($data['line'][0])){
			/*if(is_numeric($data['line'][0]) && is_numeric($data['line'][1]) && is_numeric($data['line'][2])){	
				$orden = Orden::find(trim($data['line'][0]).trim($data['line'][1]).trim($data['line'][2]));
        		if ($orden) {	
        			if($estadoProgramado = Importer::estadoOrden($orden->id, $data['atributos']['fecha'], $programada, $sypelc, $data['line'][14])){
        				$resultado->increment('creadas');
        			}
        			else{
        				$resultado->increment('actualizadas');	
        			}
        		}	
        		else{
        			$resultado->increment('noEncontradas');	
        		}
			}*/
			//else if(is_numeric($data['line'][0])){
    			$orden = Orden::find($data['line'][0]);
	        	if ($orden ) { // Busca si ya existe la orden
		       		if(Importer::estadoOrden($orden->id, $data['atributos']['fecha'], $programada, $sypelc, $data['line'][1])){
	        			$resultado->increment('creadas');
		       		}
        			else{
        				$resultado->increment('actualizadas');	
        			}
	        	}
	        	else{
        			$resultado->increment('noEncontradas');	
        		}		
	        //}
	        /*else{
	        	$resultado->increment('descartadas');	
        		Importer::descartada($resultado->id, $data['line'][0], 1);	*/
	       // }
        }
        else{
        	$resultado->increment('descartadas');	
        	Importer::descartada($resultado->id, $data['line'][0], 1);
        }
        $resultado->save();
        $job->delete();
	}
}