<?php

class RevisionSypelc extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	
	protected $table = 'revisionSypelc';
	//protected $fillable = array('id', 'ubicacionUR', 'valorUnitario', 'puntos');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($fechaAtencionR , $numeroActaR , $estadoR , $idTecnicoR , $aforoR , $oficioEntregaR , $fechaEntregaOficioR , $facturaR, $idR){
		$this->fechaAtencion = $fechaAtencionR;
		$this->numeroActa = $numeroActaR;
		$this->estadoVF = $estadoR;
		$this->idTecnico = $idTecnicoR;
		if(empty ($aforoR)){
			$this->aforo = $aforoR;
		}
		$this->oficioEntrega = $oficioEntregaR;
		$this->fechaEntregaOficio = $fechaEntregaOficioR;
		$this->factura = $facturaR;		

		$this->id = $idR;
		$this->save();
	}
}