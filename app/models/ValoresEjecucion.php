<?php


class ValoresEjecucion extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	
	protected $table = 'ejecucion_has_valor';
	//protected $fillable = array('id', 'ubicacionUR', 'valorUnitario', 'puntos');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($ejecucion, $valor){
		$this->ejecucion_estadoOrden_orden_id = $ejecucion->estadoOrden_orden_id;
		$this->ejecucion_estadoOrden_fecha = $ejecucion->estadoOrden_fecha;
		$this->ejecucion_estadoOrden_estado_id = $ejecucion->estadoOrden_estado_id;
		$this->ejecucion_estadoOrden_entidad_id = $ejecucion->estadoOrden_entidad_id;

		$this->valor_id = $valor->id;
		$this->valor_ubicacionRU = $valor->ubicacionRU;
		$this->save();
	}

	public static function buscar($ejecucion, $valor){
		$valorEjecucion = ValoresEjecucion::where('ejecucion_estadoOrden_orden_id', '=', $ejecucion->estadoOrden_orden_id)
			->where('ejecucion_estadoOrden_fecha', '=', $ejecucion->estadoOrden_fecha)	
				->where('ejecucion_estadoOrden_estado_id', '=', $ejecucion->estadoOrden_estado_id)
					->where('ejecucion_estadoOrden_entidad_id', '=', $ejecucion->estadoOrden_entidad_id)
						->where('valor_id', '=', $valor->id)
							->where('valor_ubicacionRU', '=', $valor->ubicacionRU)
								->first();
		return $valorEjecucion;
	}
}