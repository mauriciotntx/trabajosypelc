<?php


class LecturaMedidor extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	
	protected $table = 'lecturaMedidor';
	protected $fillable = array('id', 'marca', 'lectura', 'estadoEI', 'idRevision');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($medidor,$ejecucion,$lectura,$estadoEI){
		if(is_numeric($lectura)){
			$this->estadoEI = $estadoEI;
			$this->ejecucion_estadoOrden_orden_id = $ejecucion->estadoOrden_orden_id;
			$this->ejecucion_estadoOrden_fecha = $ejecucion->estadoOrden_fecha;

			$this->ejecucion_estadoOrden_estado_id = $ejecucion->estadoOrden_estado_id;
			$this->ejecucion_estadoOrden_entidad_id = $ejecucion->estadoOrden_entidad_id;
			$this->medidor_serie = $medidor->serie;
			$this->medidor_marca = $medidor->marca;
			$this->lectura = $lectura;

			$this->save();
			return true;
		}
		return false;
	}
}