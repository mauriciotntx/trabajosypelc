<?php


class ImportacionOrdenes extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'importacion_has_ordenes';
	protected $fillable = array('orden_id', 'importacion_id');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public static function crear($orden_id, $importacion_id){
		$importacion_has_ordenes = new ImportacionOrdenes;
	    $importacion_has_ordenes->orden_id = $orden_id;
	    $importacion_has_ordenes->importacion_id = $importacion_id;
	    $importacion_has_ordenes->save();
	    return $importacion_has_ordenes;
	}

	public static function buscar($orden_id, $importacion_id){
		$resultado = ImportacionOrdenes::where('orden_id' , '=' , $orden_id) //Busca si ya existe el medidor
			->where('importacion_id', '=' , $importacion_id)
				->first();
		return $resultado;
	}
}