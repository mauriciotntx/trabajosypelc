<?php


class Devolucion extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	
	protected $table = 'devolucion';
	protected $fillable = array('fecha', 'motivo', 'fechacorrecion', 'corregidoSN', 'estadoOrden_id', 'estadoOrden_fecha', 'estadoOrden_estado', 'oficio', 'lugarDevolucion_id');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;


	public function crear($estadoOrden, $lugarDevolucion_id, $motivo, $corregidoSN, $fechacorreccion){
		$this->estadoOrden_orden_id = $estadoOrden->orden_id;
		$this->estadoOrden_fecha = $estadoOrden->fecha;
		$this->estadoOrden_estado_id = $estadoOrden->estado_id;
		$this->estadoOrden_entidad_id = $estadoOrden->entidad_id;
		$this->motivo = $motivo;
		$this->corregidoSN = $corregidoSN;
		$this->lugarDevolucion_id = $lugarDevolucion_id;
		if(!is_null($fechacorreccion) && Importer::validarFecha($fechacorreccion)){
			$this->fechacorreccion = $fechacorreccion;
		}
	}

	public static function buscar($estadoOrden, $fecha, $lugarDevolucion){
		$devolucion = Devolucion::where('estadoOrden_orden_id', '=', $estadoOrden->orden_id)
			->where('estadoOrden_fecha', '=', $estadoOrden->fecha)	
				->where('estadoOrden_estado_id', '=', $estadoOrden->estado_id)
					->where('estadoOrden_entidad_id', '=', $estadoOrden->entidad_id)
						->where('lugarDevolucion_id', '=', $lugarDevolucion->id)
							->where('fecha', '=', $fecha)
								->first();
		return $devolucion;
	}
}