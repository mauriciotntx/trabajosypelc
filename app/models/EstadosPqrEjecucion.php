<?php

class EstadosPqrEjecucion extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ejecucion_has_estadoPqr';
	protected $fillable = array('estadoPqr_id', 'estadoPqr_tipoSA', 'ejecucion_estadoOrden_orden_id', 'ejecucion_estadoOrden_fecha', 'ejecucion_estadoOrden_estado_id', 'ejecucion_estadoOrden_entidad_id',);
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;


	public function crear($estadoPqr, $ejecucion){
		$this->estadoPqr_id = $estadoPqr->id;
		$this->estadoPqr_tipoSA = $estadoPqr->tipoSA;
		$this->ejecucion_estadoOrden_orden_id = $ejecucion->estadoOrden_orden_id;
		$this->ejecucion_estadoOrden_fecha = $ejecucion->estadoOrden_fecha;
		$this->ejecucion_estadoOrden_estado_id = $ejecucion->estadoOrden_estado_id;
		$this->ejecucion_estadoOrden_entidad_id = $ejecucion->estadoOrden_entidad_id;
	}
}