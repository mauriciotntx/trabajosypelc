<?php

class EstadoOrden extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'estadoorden';
	protected $fillable = array('orden_id', 'estado_id', 'fecha', 'entidad_id', 'tecnico_id', 'oficio_id');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($orden_id, $fecha, $estado_id, $entidad_id){
		$this->orden_id = $orden_id;
		
		if( strlen( trim($fecha) ) ==10){
			$this->fecha = trim($fecha);
		}
		else {
			$this->fecha = '01/01/1901';
		}

		$this->estado_id = $estado_id;
		$this->entidad_id = $entidad_id;
	}

	public function crearConTecnico($orden, $fecha, $estado, $entidad, $tecnico){
		$this->crear($orden, $fecha, $estado, $entidad);
		$this->tecnico_id = $tecnico->id;

	}

	public function agregarTecnico($tecnico){
		$this->tecnico_id = $tecnico->id;
	}

	public static function buscar($orden_id, $fecha, $estado_id, $entidad_id){
		$estadoOrden = EstadoOrden::where('orden_id', '=', $orden_id) //Busca si ya existe el estado pendiente en esta orden
			->where('fecha', '=', $fecha)		
				->where('estado_id', '=', $estado_id)
					->where('entidad_id', '=', $entidad_id)
						->first();	
		return $estadoOrden;
	}

	public static function actualizarTecnico($tecnico, $estadoOrden){
		if($estadoOrden->tecnico_id != $tecnico->id){
			EstadoOrden::where('orden_id', '=', $estadoOrden->orden_id) //Actualiza la base de datos
				->where('fecha', '=', $estadoOrden->fecha)		
					->where('estado_id', '=', $estadoOrden->estado_id)
						->update(array('tecnico_id' => $tecnico->id));	
		}	
	}

	public function scopeConEstado($query, $estado){
		return $query->where('estado_id', '=', $estado);
	}

	public function scopeConEntidad($query, $entidad){
		return $query->where('entidad_id', '=', $entidad);
	}

	public function scopeEntreFecha($query, $fechaInicio, $fechaFinal){
		return $query->where('fecha', '>', $fechaInicio)
			->where('fecha', '<', $fechaFinal);
	}	

	public function scopeJoinTecnico($query){
		return $query->join('tecnico', 'tecnico_id', '=', 'tecnico.id');
	}

	public function scopeJoinPendientes($query){
		return $query->join('pendientes', 'orden_id', '=', 'pendientes.id');
	}

	public function scopeJoinOrden($query, $tipo){
		if($tipo){
			return $query->join('orden', 'orden_id', '=', 'orden.id')
				->where('tipoOrden_id' , '=' , $tipo);
		}
		return $query->join('orden', 'orden_id', '=', 'orden.id');
	}

	public function scopeJoinEjecucion($query){
		return $query->join('ejecucion', function ($join){
			$join->on('estadoorden.orden_id', '=', 'ejecucion.estadoOrden_orden_id')
				->on('estadoorden.entidad_id', '=', 'ejecucion.estadoOrden_entidad_id')
					->on('estadoorden.estado_id', '=', 'ejecucion.estadoOrden_estado_id')
						->on('estadoorden.fecha', '=', 'ejecucion.estadoOrden_fecha');
		})
			->join('ejecucion_has_valor', function ($join){
				$join->on('ejecucion.estadoOrden_orden_id', '=', 'ejecucion_has_valor.ejecucion_estadoOrden_orden_id')
					->on('ejecucion.estadoOrden_entidad_id', '=', 'ejecucion_has_valor.ejecucion_estadoOrden_entidad_id')
						->on('ejecucion.estadoOrden_estado_id', '=', 'ejecucion_has_valor.ejecucion_estadoOrden_estado_id')
							->on('ejecucion.estadoOrden_fecha', '=', 'ejecucion_has_valor.ejecucion_estadoOrden_fecha');
			})
				->join('valor', function ($join){
					$join->on('ejecucion_has_valor.valor_id', '=', 'valor.id')
						->on('ejecucion_has_valor.valor_ubicacionRU', '=', 'valor.ubicacionRU');
				});
	}
}