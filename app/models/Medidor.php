<?php


class Medidor extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'medidor';
	protected $fillable = array('serie', 'marca', 'fecha', 'tipo', 'factorMultiplicacion', 'tipoEnergia');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;


	public function crear($serie, $marca){
		$this->serie = $serie;
		$this->marca = $marca;
	}

	public function agregarTipoFactorEnergia($tipo, $factorMultiplicacion, $tipoEnergia){
		$this->tipo = trim($tipo);
		$this->factorMultiplicacion = trim($factorMultiplicacion);
		$this->tipoEnergia = trim($tipoEnergia);
	}

	public static function buscar($serie, $marca){
		$medidor = Medidor::where('marca' , '=' , $marca) //Busca si ya existe el medidor
			->where('serie', '=' , $serie)
				->first();
		return $medidor;
	}
}