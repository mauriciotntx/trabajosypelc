<?php

class Importer{

    public static function tipoPQR($tipo_id, $dependencia_id, $accion_id){
        $tipoPQR = TipoPQR::buscar($tipo_id, $dependencia_id, $accion_id);
        if(!$tipoPQR){
            $tipo = Tipo::find($tipo_id);
            if(!$tipo){
                $tipo = new Tipo;
                $tipo->id = $tipo_id;
                $tipo->save();
             }

            $dependencia = Dependencia::find($dependencia_id);
            if(!$dependencia){
                $dependencia = new Dependencia;
                $dependencia->id = $dependencia_id;
                $dependencia->save();
            }

            $accion = Accion::find($accion_id);
            if(!$accion){
                $accion = new Accion;
                $accion->id = $accion_id;
                $accion->save();
            }

            $tipoPQR = new TipoPQR;
            $tipoPQR->crear($tipo, $dependencia, $accion, null);
            $tipoPQR->save();
        }
        return $tipoPQR;
    }

    public static function quitarAcentos($palabra){
       return  utf8_decode(str_replace('Ñ', 'N',strtr(utf8_decode($palabra), 'aeiouÁÉÍÓÚ', 'aeiouAEIOU')));
    }


    public static function validarFecha($fecha, $formato = "d/m/Y"){
        $d = DateTime::createFromFormat($formato, $fecha);
        return $d && $d->format($formato) == $fecha;
    }

    public static function subirArchivo($file){
        $destinationPath ='/var/www/campanas/public/uploads';
        $filename = str_random(8).''.$file->getClientOriginalName();
        $uploadSuccess = Input::file('archivo')->move($destinationPath, $filename);
        if( $uploadSuccess ){
            return $ubicacion = $destinationPath.'/'.$filename;
        }
        else {
            return null;
        }
    }

    public static function archivo($file, $tipo, $atributos = null){
        $tipoArchivo = Archivo::find($tipo);
        if($file){ 
            if($ubicacion = Importer::subirArchivo($file)){ // Sube el archivo al servidor
                ini_set('auto_detect_line_endings', true);
                if (is_file($ubicacion) AND is_readable($ubicacion)) {
                    if (($handle = fopen($ubicacion, "r")) !== false) {
                        $id = $tipoArchivo->id.str_random(8);
                        $resultado = new ResultadoImportacion;
                        $resultado->id = $id;
                        $resultado->tipoarchivo = $tipoArchivo->id;
                        $resultado->save();
                        $contador = 0;

                        while (($line = fgetcsv($handle, null, ';')) !== false) {
                            if($contador <= 10){
                                if(count($line)<$tipoArchivo->lineamenor || count($line)>$tipoArchivo->lineamayor){
                                    return array('error' => true, 'mensaje' => 'El Archivo no contiene el numero de lineas correcto. Este contiene '.count($line).' y debería estar entre '.$tipoArchivo->lineamenor. ' y '.$tipoArchivo->lineamayor);
                                }
                                $contador++;
                            }
                            $cont = count($line);
                            for($i=0 ; $i<$cont; $i++){
                                $line[$i] = htmlentities($line[$i], ENT_QUOTES | ENT_IGNORE, "UTF-8");
                            }

                            Queue::push('MyQueue@'.$tipoArchivo->funcionImportar, array('resultado' => $id, 'line' => $line, 'atributos' => $atributos));
                            //MyQueue::revisiones('hola' , array('resultado' => $id, 'line' => $line, 'atributos' => $atributos));
                        }
                    }
                }
            }
        }
        return array('error' => false, 'mensaje' => 'Archivo subido exitosamente', 'id' => $id);
    }

    public static function obtenerFechaArchivo($nombreArchivo){ //Obtiene la fecha del nombre de un archivo. El formato del nombre debe ser 00-00-0000.csv
        $fecha= explode('.', $nombreArchivo);
        $partesFecha = explode('-', $fecha[0]);
        $fechaReal = $partesFecha[0].'/'.$partesFecha[1].'/'.$partesFecha[2];
        return $fechaReal;
    }

    public static function lecturaMedidor($estado, $serie, $marca, $lectura, $ejecucion){
        if (is_numeric($serie) && $marca){
            $medidor = Medidor::buscar($serie, $marca);
            if(!$medidor){
                $medidor = new Medidor;
                $medidor->crear($serie, $marca);
                $medidor->save();
            }
            $lecturaMedidor = new LecturaMedidor;
            $lecturaMedidor->crear($medidor, $ejecucion, $lectura, $estado);
        }
    }  

    public static function cliente($cuentaCliente, $municipio, $servicio, $ubicacionUR, $ciclo, $ruta, $nombre, $direccion, $nodo, $macro){
        if(is_numeric($cuentaCliente)){
            $cliente = Cliente::find($cuentaCliente); //Busca si ya existe el cliente
            if(!$cliente){ //Si no existe el cliente

                $cliente = new Cliente; //Crea el cliente
                $cliente->crear($cuentaCliente, $municipio, $servicio, $ubicacionUR); 
                if($ciclo && $ruta && $nombre && $direccion){
                    $cliente->agregarCicloRutaNombreDireccion($ciclo, $ruta, $nombre, $direccion);
                }
                else if($ciclo && $nombre && $direccion){
                    $cliente->agregarCicloNombreDireccion($ciclo, $nombre, $direccion);    
                }
                else if($nombre && $direccion){
                    $cliente->agregarNombreDireccion($nombre, $direccion);     
                }
                else if($direccion){
                    $cliente->direccion = $direccion;
                }

                if($nodo){
                    $cliente->numeroNodo = $nodo;
                }
                if(is_numeric($macro)){
                    $cliente->macro = $macro;
                }
                $cliente->save();

            }
            /*else{ 
                if($nodo){
                    Cliente::actualizarNodo($cliente, $nodo);
                }
                if(is_numeric($macro)){
                    Cliente::actualizarMacro($cliente, $macro);    
                }
            }*/
            return $cliente;
        }
        return null;
    }

    public static function clientes($cuentaCliente, $municipio_id, $servicio_id, $ubicacionUR){
        $cliente = Cliente::find($cuentaCliente); //Busca si ya existe el cliente
        if(!$cliente){ //Si no existe el cliente
            $cliente = new Cliente; //Crea el cliente
            $cliente->crear($cuentaCliente, $municipio_id, $servicio_id, $ubicacionUR); 
            $cliente->save();
        }
        else{
            $cliente->ubicacionUR = $ubicacionUR;
            $cliente->save();
        }
        return $cliente->id;
    }

    public static function clienteEmsa($cuentaCliente, $municipio_id, $servicio_id, $ubicacionUR, $nombre, $direccion,$ciclo, $ruta, $nodo){
        $cliente = Cliente::find($cuentaCliente); //Busca si ya existe el cliente
        if(!$cliente){ //Si no existe el cliente
            $cliente = new Cliente; //Crea el cliente
            $cliente->crear($cuentaCliente, $municipio_id, $servicio_id, $ubicacionUR); 
            if($nombre && $direccion){
                $cliente->agregarNombreDireccion($nombre, $direccion);
            }
            if($ciclo && $ruta && $nodo){
                $cliente->agregarCicloRutaNodo($ciclo, $ruta, $nodo);    
            }
            $cliente->save();
        }
        else if(!$cliente->direccion || !$cliente->nombre){
            $cliente->direccion = $direccion;
            $cliente->nombre = $nombre;
            $cliente->save();
        }
        return $cliente->id;
    }
 
    public static function medidor($serie, $marca, $tipo, $factorMultiplicacion, $tipoEnergia){
        if (is_numeric($serie) && $marca){
            $medidor = Medidor::buscar($serie, $marca);
            if(!$medidor){ //Si no existe el medidor
                $medidor = new Medidor; //Crea el medidor
                $medidor->crear($serie, $marca);
                if($tipo && $factorMultiplicacion && $tipoEnergia){
                    $medidor->agregarTipoFactorEnergia($tipo, $factorMultiplicacion, $tipoEnergia);
                }
                $medidor->save();
            }
            return $medidor;  
        } 
        return null;                   
    }

    public static function cliente_has_medidor($medidor, $cliente_id){
        $cliente_has_medidor = ClienteMedidor::buscar($medidor, $cliente_id);
        if(!$cliente_has_medidor){ //Si aún no estan asociados
            $cliente_has_medidor = new ClienteMedidor;   //Asocia el medidor con el cliente
            $cliente_has_medidor->crear($medidor, $cliente_id);
            $cliente_has_medidor->save();
        }                         
    }

    public static function estadoOrden($orden_id, $fecha, $estado_id, $entidad_id, $tecnico_n){
        $estadoOrden = EstadoOrden::buscar($orden_id,  $fecha, $estado_id, $entidad_id);
        if(!$estadoOrden){
            $estadoOrden = new EstadoOrden;
            $estadoOrden->crear($orden_id, $fecha, $estado_id, $entidad_id); //Crea el estado pendiente para esta orden
            $tecnico = Tecnico::buscar($tecnico_n);
            if($tecnico){
                $estadoOrden->agregarTecnico($tecnico);
            }
            $estadoOrden->save();
            return $estadoOrden;
        }
        return null;
    }

    public static function revision($numOrden, $cliente_id, $numProyecto){
        if(!is_numeric($numProyecto)){
            $numProyecto = 0;
        }
        $proyecto = Proyecto::find($numProyecto);
        if($proyecto){
            $orden = new Orden;
            $orden->crearRevision($numOrden, $cliente_id, $proyecto->id);
            $orden->save();
            return $orden;
        }  
    }

    public static function solicitud($numOrden, $cliente, $numProyecto){
        if(is_numeric($numProyecto)){
            $proyecto = Proyecto::find($numProyecto);
            if($proyecto){
                $orden = new Orden;
                $orden->crearRevision($numOrden, $cliente, $proyecto);
                $orden->save();
                return $orden;
            }
        }
    }

    public static function ejecucion($estadoOrden, $estadoFV, $acta, $aforo){
        $ejecucion = new Ejecucion;
        $ejecucion->crear($estadoOrden);
        if($estadoFV){
            $ejecucion->estadoFV = trim($estadoFV);
        }
        if(is_numeric(trim($aforo))){
            $ejecucion->aforo = $aforo;
        }
        if($acta){
            $ejecucion->acta = trim($acta);
        }
        $ejecucion->save();
        return $ejecucion;
    }

    public static function ordenPqr($solicitud, $consecutivo, $tipoPQR, $direccion, $observacion){
        if(!$ordenPqr = OrdenPQR::buscar($solicitud, $consecutivo, $tipoPQR)){
            $ordenPqr = new OrdenPQR;
            $ordenPqr -> crear($solicitud, $consecutivo, $tipoPQR);
            if($direccion && $observacion){
                $ordenPqr->agregarDireccionObservacion($direccion, $observacion);
            }
            $ordenPqr->save();
        }
        return $ordenPqr;
    }

    public static function ejecucion_has_estadoPqr($ejecucion, $estadoPqr_id, $estadoPqr_tipo){
        if($estadoPqr = EstadoPqr::buscar($estadoPqr_id, $estadoPqr_tipo)){
            $ejecucion_has_estadoPqr = new EstadosPqrEjecucion;
            $ejecucion_has_estadoPqr->crear($estadoPqr, $ejecucion);  
            $ejecucion_has_estadoPqr->save();    
        }
    }

    public static function descartada($id, $idOrden, $motivo_id){
    $descartada = Descartada::buscar($id, $idOrden, $motivo_id);
        if($descartada){
            $descartada->increment('veces');    
        }
        else{
            $descartada = new Descartada;
            $descartada->id=$id;
            $descartada->idOrden = $idOrden;
            $descartada->motivo_id = $motivo_id;    
            $descartada->save();
        }
    }
}