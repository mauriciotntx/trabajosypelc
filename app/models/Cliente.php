<?php


class Cliente extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cliente';
	protected $fillable = array('id', 'municipio_id', 'direccion', 'ubicacionUR', 'macro', 'servicio_id', 'nombre', 'numeroNodo');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function hola(){

	}

	public function crear($id, $municipio_id, $servicio_id, $ubicacionUR){
		if(is_numeric($id)){
			$this->id = $id;
			$this->municipio_id = $municipio_id;
			$this->servicio_id = $servicio_id;
			$this->ubicacionUR = trim($ubicacionUR);
			return true;
		}
		return false;
	}

	public function crearNombreDireccion($id, $municipio, $servicio, $ubicacionUR, $nombre, $direccion){
		if($this->crear($id, $municipio, $servicio, $ubicacionUR)){
			$this->nombre = $nombre;
			$this->direccion = $direccion;
			return true;
		}
		return false;
	}

	public function crearNodoMacroDireccion($id, $municipio, $servicio, $ubicacionUR, $nodo, $macro, $direccion){
		if($this->crear($id, $municipio, $servicio, $ubicacionUR)){
			if($nodo){
				$this->numeroNodo = trim($nodo);
			}
			if($macro){
				$this->macro = trim($macro);
			}
			if($direccion){
				$this->direccion = $direccion;
			}
			return true;
		}
		return false;
	}

	public function crearCicloNombreDireccion($id, $municipio, $servicio, $ubicacionUR, $ciclo, $nombre, $direccion){
		if($this->crear($id, $municipio, $servicio, $ubicacionUR)){
			$this->ciclo = trim($ciclo);
			$this->nombre = $nombre;
			$this->direccion = $direccion;
			return true;
		}
		return false;
	}

	public function crearCicloRutaNombreDireccion($id, $municipio, $servicio, $ubicacionUR, $ciclo, $ruta, $nombre, $direccion){
		if($this->crear($id, $municipio, $servicio, $ubicacionUR)){
			$this->ciclo = trim($ciclo);
			$this->ruta = trim($ruta);
			$this->nombre = $nombre;
			$this->direccion = $direccion;
			return true;
		}
		return false;
	}

	public static function actualizarNodo($cliente, $nodo){
		Cliente::where('id', '=', $cliente->id) //Actualiza la base de datos
			->update(array('numeroNodo' => $nodo));
	}

	public static function actualizarMacro($cliente, $macro){
		Cliente::where('id', '=', $cliente->id) //Actualiza la base de datos
			->update(array('macro' => $macro));
	}

	public static function actualizarNodoMacroDireccion($cliente, $nodo,  $macro, $direccion){
		if($nodo && $macro && $direccion){
		Cliente::where('id', '=', $cliente->id) //Actualiza la base de datos
			->update(array('numeroNodo' => $nodo, 'macro' => $macro, 'direccion' => $direccion));
		}
	}

	public function agregarCicloRutaNombreDireccion($ciclo, $ruta, $nombre, $direccion){
		$this->ciclo = trim($ciclo);
		$this->ruta = trim($ruta);
		$this->nombre = $nombre;
		$this->direccion = $direccion;	
	}

	public function agregarCicloRutaNombreDireccionNodo($ciclo, $ruta, $nombre, $direccion, $nodo){
		$this->ciclo = trim($ciclo);
		$this->ruta = trim($ruta);
		$this->nombre = $nombre;
		$this->direccion = $direccion;	
		$this->numeroNodo = $nodo;
	}

	public function agregarCicloRutaNodo($ciclo, $ruta, $nodo){
		$this->ciclo = trim($ciclo);
		$this->ruta = trim($ruta);
		$this->numeroNodo = $nodo;
	}

	public function agregarCicloNombreDireccion($ciclo, $nombre, $direccion){
		$this->ciclo = trim($ciclo);
		$this->nombre = $nombre;
		$this->direccion = $direccion;	
	}
	public function agregarNombreDireccion($nombre, $direccion){
		$this->nombre = $nombre;
		$this->direccion = $direccion;	
	}

	public static function prueba($data)
	{
		$prueba = new Prueb;
			$prueba->ddd = 56;
			$prueba->save();
	}
}