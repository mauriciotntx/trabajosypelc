<?php


class IrregularidadesEjecucion extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	
	protected $table = 'ejecucion_has_irregularidad';
	protected $fillable = array('ejecucion_estadoOrden_orden_id', 'ejecucion_estadoOrden_fehca','ejecucion_estadoOrden_estado_id', 'irregularidad_id');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($ejecucion, $irregularidad){
		$this->ejecucion_estadoOrden_orden_id = $ejecucion->estadoOrden_orden_id;
		$this->ejecucion_estadoOrden_fecha = $ejecucion->estadoOrden_fecha;
		$this->ejecucion_estadoOrden_estado_id = $ejecucion->estadoOrden_estado_id;
		$this->ejecucion_estadoOrden_entidad_id = $ejecucion->estadoOrden_entidad_id;
		
		$this->irregularidad_id = $irregularidad->id;
	
		$this->save();
	}

	public static function buscar($ejecucion, $irregularidad){
		$irregularidadEjecucion = IrregularidadesEjecucion::where('ejecucion_estadoOrden_orden_id', '=', $ejecucion->estadoOrden_orden_id)
			->where('ejecucion_estadoOrden_fecha', '=', $ejecucion->estadoOrden_fecha)	
				->where('ejecucion_estadoOrden_estado_id', '=', $ejecucion->estadoOrden_estado_id)
					->where('ejecucion_estadoOrden_entidad_id', '=', $ejecucion->estadoOrden_entidad_id)
						->where('irregularidad_id', '=', $irregularidad->id)
							->first();
		return $irregularidadEjecucion;
	}
}