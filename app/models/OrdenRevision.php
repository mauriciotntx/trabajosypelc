<?php


class OrdenRevision extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ordenRevision';
	protected $fillable = array('id', 'idCliente', 'idCampana','fechaGeneracion');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($idR, $idClienteR, $idProyectoR,$campanaR, $fechaGeneracionR){
		$this->id = $idR;
		$this->idCliente = $idClienteR;
		$this->idProyecto = $idProyectoR;
		$this->fechaGeneracion = $fechaGeneracionR;
		if($campanaR){
			$this->idCampana = $campanaR->id;	
		}
		$this->save();
	}
}