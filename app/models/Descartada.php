<?php


class Descartada extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'descartada';
	protected $fillable = array('id');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;


	public static function buscar($id, $idOrden, $motivo_id){
		$descartada = Descartada::where('id', '=', $id) //Busca si ya existe el estado pendiente en esta orden
			->where('idOrden', '=', $idOrden)		
				->where('motivo_id', '=', $motivo_id)
						->first();	
		return $descartada;
	}
}