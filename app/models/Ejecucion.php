<?php


class Ejecucion extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ejecucion';
	protected $fillable = array('estadoOrden_entidad_id', 'estadoOrden_orden_id', 'estadoOrden_fecha', 'estadoOrden_estado_id', 'estadoFV', 'acta');
	public $timestamps = false;
	public $incrementing = false; 
	public $errors;

	public function crear($estadoOrden){
		$this->estadoOrden_orden_id = $estadoOrden->orden_id; 
		$this->estadoOrden_fecha = $estadoOrden->fecha;
		$this->estadoOrden_estado_id = $estadoOrden->estado_id;
		$this->estadoOrden_entidad_id = $estadoOrden->entidad_id;
	}

	public function agregarEstadoActaAforo($estadoFV, $acta, $aforo){
		$this->estadoFV = $estadoFV;
		$this->acta = $acta;
		$this->aforo = $aforo;
	}

	public static function buscarPorActa($orden, $acta, $entidad){
		if($orden && $acta){
			$ejecucion = Ejecucion::where('estadoOrden_orden_id', '=', $orden->id)	
				->where('estadoOrden_entidad_id', '=', $entidad->id)	
					->where('acta', '=', $acta)
						->get();
			if(count($ejecucion) == 1){
				$ejecucion = Ejecucion::where('estadoOrden_orden_id', '=', $orden->id)	
				->where('estadoOrden_entidad_id', '=', $entidad->id)	
					->where('acta', '=', $acta)
						->first();
				return $ejecucion;
			}
		}
	}

	public static function buscar($estadoOrden){
		$ejecucion = Ejecucion::where('estadoOrden_orden_id', '=', $estadoOrden->orden_id)
			->where('estadoOrden_fecha', '=', $estadoOrden->fecha)
				->where('estadoOrden_estado_id', '=', $estadoOrden->estado_id)
					->first();
		return $ejecucion;
	}

	public static function buscarComoSea($acta, $entidad_id){
		if($acta){
			$ejecucion = Ejecucion::where('estadoOrden_entidad_id', '=', $entidad_id)	
					->where('acta', '=', $acta)
						->get();
			if(count($ejecucion) == 1){
				$ejecucion = Ejecucion::where('estadoOrden_entidad_id', '=', $entidad_id)	
						->where('acta', '=', $acta)
							->first();
				return $ejecucion;
			}
		}
		else if($acta){
			$ejecucion = Ejecucion::where('acta', '=', $acta)
				->get();
			if(count($ejecucion) == 1){
				return $ejecucion;
			}
		}
		return false;
	}
}