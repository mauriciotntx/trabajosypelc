<?php
    class SimpleCsv
    {
        public static function import($filePath, $delimiter = ",")
        {
            $data = false;
            ini_set('auto_detect_line_endings', true);
            if (is_file($filePath) AND is_readable($filePath)) {
                if (($handle = fopen($filePath, "r")) !== false) {
                    $data = array();
                    while (($line = fgetcsv($handle, null, $delimiter)) !== false) {
                        $data[] = $line;
                    }
                }
            }
            return $data;
        }
        public static function export($data, $delimiter = ";")
        {
            ob_start();
            $fp = fopen("php://output", 'w');
            foreach ($data as $row) {
                fputcsv($fp, $row, $delimiter, '"');
            }
            fclose($fp);
            return ob_get_clean();
        }
    }

